package kalkulator;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * @author rp386037
 */
public class Kalkulator extends JFrame {
    JLabel etykieta;
    KalkulatorModel kalkulatorModel;
    private final int WINDOW_HEIGHT = 300;
    private final int WINDOW_WIDTH = 550;
    private final float FONT_SIZE = 25.0f;
    
    //klasa pomocnicza przycisku, ktora wysyla jego nazwe do funkcji pressed
    class MyButton extends JButton {
        class MyButtonPressed implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e) {
                pressed(e.getActionCommand());
            }
        }
        public MyButton(String nazwa) {
            super(nazwa);
            this.addActionListener(new MyButtonPressed());
            //wylaczenie mozliwosci focusowania przyciskow pozwala bez przeszkod wpisywac tekst
            this.setFocusable(false);
        }
    }

    //funkcja obslugujaca wciskanie klawiszy i wpisywanie tekstu
    public void pressed(String komenda) {
        switch (komenda) {
            case "=":
                this.etykieta.setText(kalkulatorModel.policz());
                break;
            case "undo":
                this.etykieta.setText(kalkulatorModel.cofnij());
                break;
            case "redo":
                this.etykieta.setText(kalkulatorModel.ponow());
                break;
            case "clear all":
                this.etykieta.setText(kalkulatorModel.kasujWszystko());  
                break;
            case "clear last":
                this.etykieta.setText(kalkulatorModel.kasujZnak());
                break;
            //jesli nic z powyzszych nie pasuje to zakladamy, ze jest to cyfra lub +, -, *, /
            default:
                this.etykieta.setText(kalkulatorModel.dopisz(komenda));
                break;
        }
    }

    private void utworzGUI() {
        this.setDefaultCloseOperation(3);
        //obsluga przyciskow, backspace - kasuj znak, enter - oblicz
        addKeyListener(new KeyListener(){
            @Override
            public void keyTyped(KeyEvent e) {
                switch ((int)e.getKeyChar()) {
                    case 8:
                        pressed("clear last");
                        break;
                    case 10:
                        pressed("=");
                        break;
                    default:
                        pressed(String.valueOf(e.getKeyChar()));
                }
            }

            @Override
            public void keyPressed(KeyEvent e) { }

            @Override
            public void keyReleased(KeyEvent e) { }
        });
        
        JPanel gui = new JPanel(new BorderLayout(20, 20));
        gui.setBorder(new EmptyBorder(5,5,5,5));
        
        this.etykieta = new JLabel("");
        this.etykieta.setPreferredSize(new Dimension(1, 30));
        this.etykieta.setFont(this.etykieta.getFont().deriveFont(this.FONT_SIZE));
        gui.add(this.etykieta, BorderLayout.NORTH);
        
        JPanel buttonPanel = new JPanel(new GridLayout(4, 5, 5, 5));
        buttonPanel.add(new MyButton("7"));
        buttonPanel.add(new MyButton("8"));
        buttonPanel.add(new MyButton("9"));
        buttonPanel.add(new MyButton("+"));
        buttonPanel.add(new MyButton("undo"));
        buttonPanel.add(new MyButton("4"));
        buttonPanel.add(new MyButton("5"));
        buttonPanel.add(new MyButton("6"));
        buttonPanel.add(new MyButton("-"));
        buttonPanel.add(new MyButton("redo"));
        buttonPanel.add(new MyButton("1"));
        buttonPanel.add(new MyButton("2"));
        buttonPanel.add(new MyButton("3"));
        buttonPanel.add(new MyButton("*"));
        buttonPanel.add(new MyButton("clear last"));
        buttonPanel.add(new MyButton("0"));
        buttonPanel.add(new MyButton(" "));
        buttonPanel.add(new MyButton("="));
        buttonPanel.add(new MyButton("/"));
        buttonPanel.add(new MyButton("clear all"));
        gui.add(buttonPanel, BorderLayout.CENTER);
        
        this.add(gui);
        this.setSize(this.WINDOW_WIDTH, this.WINDOW_HEIGHT);
        this.setVisible(true);
        this.setFocusable(true);
        this.pack();
        this.requestFocus();
    }

    public Kalkulator() {
        super("Kalkulator");
        this.kalkulatorModel = new KalkulatorModel();
        this.utworzGUI();
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new Kalkulator();
            }
        });
    }
}