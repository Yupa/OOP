package kalkulator;

import java.util.Stack;

/**
 * @author rp386037
 */
public class KalkulatorModel {
    //stos kolejno wywolywanych komend
    private Stack <String> komendy;
    //stos kolejno cofanych komend
    private Stack <String> komendyCofniete;
    private String data;
    
    public KalkulatorModel() {
        this.data = "";
        this.komendy = new Stack<>();
        this.komendyCofniete = new Stack<>();
        //pusty ciag znakow jako wartownik
        komendy.add(data);
    }
    
    public String dopisz(String znak) {
        data += znak;
        komendyCofniete.clear();
        komendy.add(data);
        return data;
    }

    public String kasujZnak() {
        //sprawdzamy czy tekst nie jest pusty
        if (!data.equals("")) {
            data = data.substring(0, data.length() - 1);
            komendy.add(data);
            komendyCofniete.clear();
        }
        return data;
    }
    
    public String kasujWszystko() {
        //jesli nie ma zadnego znaku to nie dodajemy tej akcji na stos komend
        if (!data.equals("")) {
            data = "";
            komendy.add(data);
            komendyCofniete.clear();
        }
        return data;
    }
    
    public String policz() {
        //jesli pusty tekst to nic nie robimy
        if (!data.equals("")) {
            //stos liczb - uzywany przy obliczaniu wyniku ONP
            Stack <Integer> expression = new Stack<>();
            //obecnie czytana w tekscie liczba
            Integer liczba = null;
            //inty pomocnicze do obslugi stosu
            Integer l1, l2;
            for (int i = 0; i < data.length(); i++) {
                switch(data.charAt(i)) {
                    case '+':
                        l2 = expression.pop();
                        l1 = expression.pop();
                        expression.add(l1 + l2);
                        break;
                    case '-':
                        //sprawdzanie czy minus oznacza odejmowanie czy liczbe ujemna
                        if (i < data.length()-1 && data.charAt(i+1) != ' ') {
                            liczba = (int)data.charAt(i+1) - 48;
                            liczba *= -1;
                            i++;
                        }
                        else {
                            l2 = expression.pop();
                            l1 = expression.pop();
                            expression.add(l1 - l2);
                        }
                        break;
                    case '*':
                        l2 = expression.pop();
                        l1 = expression.pop();
                        expression.add(l1 * l2);
                        break;
                    case '/':
                        l2 = expression.pop();
                        l1 = expression.pop();
                        expression.add(l1 / l2);
                        break;
                    case ' ':
                        //jesli wczytalismy liczbe to odkladamy ją na stos
                        if (liczba != null)
                            expression.add(liczba);
                        liczba = null;
                        break;
                    //jesli nic z powyzszych to zakladamy, ze wczytujemy cyfre
                    default:
                        if (liczba == null)
                            liczba = (int)data.charAt(i) - 48;
                        else {
                            liczba *= 10;
                            if (liczba < 10)
                                liczba -= ((int)data.charAt(i) - 48);
                            else
                                liczba += ((int)data.charAt(i) - 48);
                        }
                        break;
                }
            }
            //jesli wyrazenie sklada sie tylko z jednej liczby bez operatorow to ja zwracamy
            if (liczba != null)
                expression.add(liczba);
            //zbieramy ostatni (jedyny przy poprawnych danych wejsciowych) element i go zwracamy
            data = "" + expression.pop();
            komendy.add(data);
            komendyCofniete.clear();
        }
        return data;
    }
    
    public String cofnij() {
        //sprawdzamy czy da sie cofnac komende
        if (komendy.size() > 1) {
            komendyCofniete.add(this.data);
            komendy.pop();
            data = komendy.peek();
        }
        return data;
    }
    
    public String ponow() {
        //patrzymy czy jest cos cofniete
        if (!komendyCofniete.empty()) {
            data = komendyCofniete.pop();
            komendy.add(data);
        }
        return data;
    }
    
}