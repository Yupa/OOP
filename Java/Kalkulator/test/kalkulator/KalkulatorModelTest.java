package kalkulator;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author rp386037
 */
public class KalkulatorModelTest {

    /**
     * Test of dopisz method, of class KalkulatorModel.
     */
    @Test
    public void testDopisz() {
        System.out.println("dopisz");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("1");
        String result = instance.dopisz("2");
        String expResult = "12";
        assertEquals(expResult, result);;
    }

    /**
     * Test of kasujZnak method, of class KalkulatorModel.
     */
    @Test
    public void testKasujZnak() {
        System.out.println("kasujZnak");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("1");
        instance.dopisz("2");
        String result = instance.kasujZnak();
        String expResult = "1";
        assertEquals(expResult, result);
    }

    /**
     * Test of kasujWszystko method, of class KalkulatorModel.
     */
    @Test
    public void testKasujWszystko() {
        System.out.println("kasujWszystko");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("1");
        instance.dopisz(" ");
        instance.dopisz("2");
        instance.dopisz(" ");
        instance.dopisz("+");
        String result = instance.kasujWszystko();
        String expResult = "";
        assertEquals(expResult, result);
    }

    /**
     * Test of policz method, of class KalkulatorModel.
     */
    @Test
    public void testPolicz() {
        System.out.println("policz");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("8 7 + 3 / 14 3 - 4 * + 7 /");
        String result = instance.policz();
        String expResult = "7";
        assertEquals(expResult, result);
    }
    
    /**
     * Test of cofnij method, of class KalkulatorModel.
     */
    @Test
    public void testCofnij() {
        System.out.println("cofnij");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("1");
        instance.dopisz(" ");
        instance.dopisz("2");
        instance.dopisz(" ");
        instance.dopisz("+");
        instance.policz();
        String result = instance.cofnij();
        String expResult = "1 2 +";
        assertEquals(expResult, result);
    }

    /**
     * Test of ponow method, of class KalkulatorModel.
     */
    @Test
    public void testPonow() {
        System.out.println("ponow");
        KalkulatorModel instance = new KalkulatorModel();
        instance.dopisz("1");
        instance.dopisz(" ");
        instance.dopisz("2");
        instance.dopisz(" ");
        instance.dopisz("+");
        instance.policz();
        instance.cofnij();
        String result = instance.ponow();
        String expResult = "3";
        assertEquals(expResult, result);
    }
}
