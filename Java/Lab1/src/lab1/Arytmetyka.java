package lab1;

public class Arytmetyka {

    
    public static void main(String[] args) {
        int x = 14;
        int y = 63;
        System.out.println(nwd(x, y));
        System.out.println(nwd2(x, y));
    }

    private static int nwd(int x, int y) {
        if (x == 0 && y == 0) return -1;
        if (x > y) return nwd (y, x);
        if (x == 0) return y;
        else
            return nwd(y%x, x);
    }
    
    private static int nwd2(int x, int y) {
        if (x == 0 || y == 0) 
            return -1;
        if (x > y)
        {
            int tmp = y;
            y = x;
            x = tmp;
        }
        while(x != 0) {
            int tmp = y % x;
            y = x;
            x = tmp;
        }
        return y;
    }
    
}
