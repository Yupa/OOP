package wyrazeniaarytmetyczne;

import org.junit.Test;
import static org.junit.Assert.*;
import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class FabrykaWyrazenTest {

    Wyrazenie w1, w2, w3;
    double x1, x2, x3, x4;
    double EPSILON = 0.00001;

    @Test
    public void testStala() {
        System.out.println("testStala");
        w1 = stala(x2);
        assertEquals(w1.policzWartosc(x1), w1.policzWartosc(x2), EPSILON);
        w2 = stala(1);
        assertEquals(w2.policzWartosc(x1), 1, EPSILON);
    }

    @Test
    public void testZmienna() {
        System.out.println("testZmienna");
        w1 = zmienna();
        assertEquals(w1.policzWartosc(x1), x1, EPSILON);
        assertEquals(w1.policzWartosc(x2), x2, EPSILON);
        assertEquals(w1.policzWartosc(x3), x3, EPSILON);
        assertEquals(w1.policzWartosc(x4), x4, EPSILON);
    }

    @Test
    public void testSinus() {
        System.out.println("testSinus");
        x1 = 0.0;
        x2 = Math.PI / 2;
        x3 = Math.PI;
        w1 = sinus(zmienna());
        w2 = sinus(stala(x2));
        assertEquals(w1.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w1.policzWartosc(x2), 1.0, EPSILON);
        assertEquals(w1.policzWartosc(x3), 0.0, EPSILON);
        assertEquals(w2.policzWartosc(x1), 1.0, EPSILON);
    }

    @Test
    public void testCosinus() {
        System.out.println("testCosinus");
        x1 = 0.0;
        x2 = Math.PI / 2;
        x3 = Math.PI;
        w1 = cosinus(zmienna());
        w2 = cosinus(stala(x3));
        assertEquals(w1.policzWartosc(x1), 1.0, EPSILON);
        assertEquals(w1.policzWartosc(x2), 0.0, EPSILON);
        assertEquals(w1.policzWartosc(x3), -1.0, EPSILON);
        assertEquals(w2.policzWartosc(x1), -1.0, EPSILON);
    }

    @Test
    public void testLogarytm() {
        System.out.println("testLogarytm");
        x1 = 1.0;
        x2 = 1 / Math.E;
        x3 = Math.E;
        w1 = logarytm(zmienna());
        w2 = logarytm(stala(x3));
        assertEquals(w1.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w1.policzWartosc(x2), -1.0, EPSILON);
        assertEquals(w1.policzWartosc(x3), 1.0, EPSILON);
        assertEquals(w2.policzWartosc(x1), 1.0, EPSILON);
    }

    @Test
    public void testDodawanie() {
        System.out.println("testDodawanie");
        x1 = 0.0;
        x2 = 1.0;
        x3 = -1.0;
        w1 = zmienna();
        w2 = stala(x3);
        w3 = dodaj(w1, w2);
        assertEquals(dodaj(w3, stala(0)), w3);
        assertEquals(w3, dodaj(w3, stala(0)));
        assertEquals(w3.policzWartosc(x1), -1.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), 0.0, EPSILON);
        assertEquals(w3.policzWartosc(x3), -2.0, EPSILON);
    }

    @Test
    public void testOdejmowanie() {
        System.out.println("testOdejmowanie");
        x1 = 0.0;
        x2 = 1.0;
        x3 = -1.0;
        w1 = zmienna();
        w2 = stala(x3);
        w3 = odejmij(w1, w2);
        assertEquals(odejmij(w3, stala(0)), w3);
        assertEquals(w3, odejmij(w3, stala(0)));
        assertEquals(w3.policzWartosc(x1), 1.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), 2.0, EPSILON);
        assertEquals(w3.policzWartosc(x3), 0.0, EPSILON);
    }

    @Test
    public void testMnozenie() {
        System.out.println("testMnozenie");
        x1 = 0.0;
        x2 = 1.0;
        x3 = -1.0;
        x4 = 2.0;
        w1 = zmienna();
        w2 = stala(x3);
        w3 = razy(w1, w2);
        assertEquals(razy(w3, stala(1)), w3);
        assertEquals(w3, razy(w3, stala(1)));
        assertEquals(w3.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), -1.0, EPSILON);
        assertEquals(w3.policzWartosc(x3), 1.0, EPSILON);
        assertEquals(w3.policzWartosc(x4), -2.0, EPSILON);
    }

    @Test
    public void testDzielenie() {
        System.out.println("testDzielenie");
        x1 = 0.0;
        x2 = 1.0;
        x3 = -1.0;
        x4 = 2.0;
        w1 = zmienna();
        w2 = stala(x3);
        w3 = podziel(w1, w2);
        assertEquals(podziel(w3, stala(1)), w3);
        assertEquals(w3, podziel(w3, stala(1)));
        assertEquals(w3.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), -1.0, EPSILON);
        assertEquals(w3.policzWartosc(x3), 1.0, EPSILON);
        assertEquals(w3.policzWartosc(x4), -2.0, EPSILON);
        assertEquals(podziel(w2, w1).policzWartosc(x4), -0.5, EPSILON);
    }

    @Test
    public void testPochodna() {
        System.out.println("testPochodna");
        x1 = 1.0;
        w1 = zmienna();
        w2 = stala(x1);
        w3 = razy(w1, w2).policzPochodna();
        assertEquals(stala(1), w3);
        assertEquals(w3.policzWartosc(x1), 1.0, EPSILON);

        w1 = zmienna();
        w2 = cosinus(w1);
        w3 = w2.policzPochodna();
        x1 = 0.0;
        x2 = Math.PI / 2;
        x3 = Math.PI;
        assertEquals(w3.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), -1.0, EPSILON);
        assertEquals(w3.policzWartosc(x3), 0.0, EPSILON);

        w1 = zmienna();
        w2 = logarytm(w1);
        w3 = w2.policzPochodna();
        x1 = 1.0;
        x2 = 2.0;
        x3 = -1.0;
        assertEquals(w3.policzWartosc(x1), 1.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), 0.5, EPSILON);
        assertEquals(w3.policzWartosc(x3), -1.0, EPSILON);

        w1 = cosinus(zmienna());
        w2 = logarytm(w1);
        w3 = w2.policzPochodna();
        x1 = 0.0;
        x2 = Math.PI / 4;
        assertEquals(w3.policzWartosc(x1), 0.0, EPSILON);
        assertEquals(w3.policzWartosc(x2), -1.0, EPSILON);
    }

    @Test
    public void testWypisz() {
        System.out.println("testWypisz");
        x1 = 0.0;
        x2 = 1.0;
        w1 = zmienna();
        w2 = stala(x3);
        // (sin x + log 1) / (cos(log x * sin 0) - cos 0)
        w3 = podziel(dodaj(sinus(zmienna()), logarytm(stala(x2))),
                odejmij(cosinus(razy(logarytm(zmienna()), sinus(stala(x1)))), cosinus(stala(x1))));
        w3.wypisz();
        // (sin x + log x) / (cos(log x * sin x) - cos x)
        w3 = podziel(dodaj(sinus(zmienna()), logarytm(zmienna())),
                odejmij(cosinus(razy(logarytm(zmienna()), sinus(zmienna()))), cosinus(zmienna())));
        w3.wypisz();
    }
}
