package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Mnozenie extends Operator {

    public Mnozenie(Wyrazenie l, Wyrazenie p) {
        super(l, p);
    }

    @Override
    public double policzWartosc(double x) {
        return lewe.policzWartosc(x) * prawe.policzWartosc(x);
    }

    // (f*g)' = f'*g + f*g'
    @Override
    public Wyrazenie policzPochodna() {
        return dodaj(razy(lewe.policzPochodna(), prawe), razy(lewe, prawe.policzPochodna()));
    }

    @Override
    public String getZnak() {
        return " * ";
    }

    @Override
    public int getPriorytet() {
        return 1;
    }
}
