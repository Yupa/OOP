package wyrazeniaarytmetyczne;

public class FabrykaWyrazen {

    public static final Stala ZERO = new Stala(0);
    public static final Stala JEDEN = new Stala(1);
    public static final Zmienna ZMIENNAX = new Zmienna();

    /* ustawiamy statyczne konstruktory wyrazen
    *  ktore upraszczaja wyrazenia podczas tworzenia
     */
    public static final Wyrazenie stala(double val) {
        // jesli wartosc stalej to 0 lub 1 to wskazujemy na odpowiednie stale
        if (val == 0) {
            return ZERO;
        }
        if (val == 1) {
            return JEDEN;
        }
        return new Stala(val);
    }

    public static final Wyrazenie zmienna() {
        return ZMIENNAX;
    }

    public static final Wyrazenie sinus(Wyrazenie wyrazenie) {
        // sin(0) = 0
        if (wyrazenie.equals(ZERO)) {
            return ZERO;
        }
        return new Sinus(wyrazenie);
    }

    public static final Wyrazenie cosinus(Wyrazenie wyrazenie) {
        // cos(0) = 1
        if (wyrazenie.equals(ZERO)) {
            return JEDEN;
        }
        return new Cosinus(wyrazenie);
    }

    public static final Wyrazenie logarytm(Wyrazenie wyrazenie) {
        // log(1) = 0
        if (wyrazenie.equals(JEDEN)) {
            return ZERO;
        }
        return new Logarytm(wyrazenie);
    }

    public static final Wyrazenie podziel(Wyrazenie lewe, Wyrazenie prawe) {
        // 0/x = 0
        if (lewe.equals(ZERO)) {
            return ZERO;
        }
        // x/1 = x
        if (prawe.equals(JEDEN)) {
            return lewe;
        }
        return new Dzielenie(lewe, prawe);
    }

    public static final Wyrazenie razy(Wyrazenie lewe, Wyrazenie prawe) {
        // 0*x = x*0 = 0
        if (lewe.equals(ZERO) || prawe.equals(ZERO)) {
            return ZERO;
        }
        // x*1 = x
        if (prawe.equals(JEDEN)) {
            return lewe;
        }
        // 1*x = x
        if (lewe.equals(JEDEN)) {
            return prawe;
        }
        return new Mnozenie(lewe, prawe);
    }

    public static final Wyrazenie dodaj(Wyrazenie lewe, Wyrazenie prawe) {
        // x + 0 = x
        if (prawe.equals(ZERO)) {
            return lewe;
        }
        // 0 + x = 0
        if (lewe.equals(ZERO)) {
            return prawe;
        }
        return new Dodawanie(lewe, prawe);
    }

    public static final Wyrazenie odejmij(Wyrazenie lewe, Wyrazenie prawe) {
        // x - 0 = x
        if (prawe.equals(ZERO)) {
            return lewe;
        }
        return new Odejmowanie(lewe, prawe);
    }
}
