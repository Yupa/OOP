package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Dodawanie extends Operator {

    public Dodawanie(Wyrazenie l, Wyrazenie p) {
        super(l, p);
    }

    @Override
    public double policzWartosc(double x) {
        return lewe.policzWartosc(x) + prawe.policzWartosc(x);
    }

    // (f + g)' = f' + g'
    @Override
    public Wyrazenie policzPochodna() {
        return dodaj(lewe.policzPochodna(), prawe.policzPochodna());
    }

    @Override
    public String getZnak() {
        return " + ";
    }

    @Override
    public int getPriorytet() {
        return 2;
    }
}
