package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Dzielenie extends Operator {

    public Dzielenie(Wyrazenie l, Wyrazenie p) {
        super(l, p);
    }

    @Override
    protected boolean czyNawiasowacPrawe() {
        return (prawe.getPriorytet() >= getPriorytet());
    }

    @Override
    public double policzWartosc(double x) {
        return lewe.policzWartosc(x) / prawe.policzWartosc(x);
    }

    // (f/g)' = (f'*g - f*g') / g*g
    @Override
    public Wyrazenie policzPochodna() {
        return podziel(
            odejmij(razy(lewe.policzPochodna(), prawe), 
                    razy(lewe, prawe.policzPochodna())),
            razy(prawe, prawe));
    }

    @Override
    public String getZnak() {
        return " / ";
    }

    @Override
    public int getPriorytet() {
        return 1;
    }
}
