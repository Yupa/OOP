package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public abstract class FunkcjaJednejZmiennej extends Wyrazenie {

    Wyrazenie wyrazenie;

    public FunkcjaJednejZmiennej(Wyrazenie w) {
        wyrazenie = w;
    }

    // zwraca wzor na pochodna funkcji od x
    protected abstract Wyrazenie getWzorNaPochodna();

    // f(g(x)) = f'(g(x)) * g'(x)
    @Override
    public Wyrazenie policzPochodna() {
        return razy(getWzorNaPochodna(), wyrazenie.policzPochodna());
    }

    @Override
    public int getPriorytet() {
        return 0;
    }
}
