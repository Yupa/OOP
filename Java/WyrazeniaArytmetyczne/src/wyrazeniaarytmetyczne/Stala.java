package wyrazeniaarytmetyczne;

public class Stala extends Wyrazenie {

    double wartosc;

    public Stala(double val) {
        wartosc = val;
    }

    @Override
    public double policzWartosc(double x) {
        return wartosc;
    }

    @Override
    public Wyrazenie policzPochodna() {
        return FabrykaWyrazen.ZERO;
    }

    @Override
    public String toString() {
        return "" + wartosc;
    }

    @Override
    public int getPriorytet() {
        return 0;
    }
}
