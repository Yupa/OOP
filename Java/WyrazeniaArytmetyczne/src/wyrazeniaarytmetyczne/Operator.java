package wyrazeniaarytmetyczne;

public abstract class Operator extends Wyrazenie {

    Wyrazenie lewe, prawe;

    public Operator(Wyrazenie l, Wyrazenie p) {
        lewe = l;
        prawe = p;
    }

    protected abstract String getZnak();

    /* priorytety nawiasowania:
       stale, zmienne, funkcje 1-arg -> 0
       mnozenie, dzielenie -> 1
       dodawanie, odejmowanie -> 2
    */
    protected boolean czyNawiasowacLewe() {
        return (lewe.getPriorytet() > getPriorytet());
    }

    protected boolean czyNawiasowacPrawe() {
        return (prawe.getPriorytet() > getPriorytet());
    }

    @Override
    public String toString() {
        String wynik = "";
        if (czyNawiasowacLewe()) {
            wynik += "(" + lewe.toString() + ")";
        } else {
            wynik += lewe.toString();
        }
        wynik += getZnak();
        if (czyNawiasowacPrawe()) {
            wynik += "(" + prawe.toString() + ")";
        } else {
            wynik += prawe.toString();
        }
        return wynik;
    }
}
