package wyrazeniaarytmetyczne;

public abstract class Wyrazenie {

    double EPSILON = 0.000001;

    public abstract double policzWartosc(double x);

    public double policzenieCalki(double lewyKoniec, double prawyKoniec) {
        double suma = 0;
        for (double i = lewyKoniec; i < prawyKoniec; i += EPSILON) {
            suma += policzWartosc(i);
        }
        return suma;
    }

    public abstract Wyrazenie policzPochodna();

    @Override
    public abstract String toString();

    public abstract int getPriorytet();

    public void wypisz() {
        System.out.println(this.toString());
    }
}
