package wyrazeniaarytmetyczne;

public class Zmienna extends Wyrazenie {

    @Override
    public double policzWartosc(double x) {
        return x;
    }

    @Override
    public Wyrazenie policzPochodna() {
        return FabrykaWyrazen.JEDEN;
    }

    @Override
    public String toString() {
        return "x";
    }

    @Override
    public int getPriorytet() {
        return 0;
    }
}
