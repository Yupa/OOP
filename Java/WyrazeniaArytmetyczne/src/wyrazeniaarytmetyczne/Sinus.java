package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Sinus extends FunkcjaJednejZmiennej {

    public Sinus(Wyrazenie w) {
        super(w);
    }

    @Override
    public Wyrazenie getWzorNaPochodna() {
        return cosinus(wyrazenie);
    }

    @Override
    public String toString() {
        return "sin(" + super.wyrazenie.toString() + ")";
    }

    @Override
    public double policzWartosc(double x) {
        return Math.sin(wyrazenie.policzWartosc(x));
    }
}
