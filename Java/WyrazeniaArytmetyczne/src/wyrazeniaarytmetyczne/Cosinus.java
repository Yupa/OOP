package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Cosinus extends FunkcjaJednejZmiennej {

    public Cosinus(Wyrazenie w) {
        super(w);
    }

    // (cos x)' = 0 - sin x
    @Override
    public Wyrazenie getWzorNaPochodna() {
        return odejmij(ZERO, sinus(wyrazenie));
    }

    @Override
    public String toString() {
        return "cos(" + super.wyrazenie.toString() + ")";
    }

    @Override
    public double policzWartosc(double x) {
        return Math.cos(wyrazenie.policzWartosc(x));
    }
}
