package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Odejmowanie extends Operator {

    public Odejmowanie(Wyrazenie l, Wyrazenie p) {
        super(l, p);
    }
    
    @Override
    protected boolean czyNawiasowacPrawe() {
        return (prawe.getPriorytet() >= getPriorytet());
    }

    @Override
    public double policzWartosc(double x) {
        return lewe.policzWartosc(x) - prawe.policzWartosc(x);
    }

    // (f - g)' = f' - g'
    @Override
    public Wyrazenie policzPochodna() {
        return odejmij(lewe.policzPochodna(), prawe.policzPochodna());
    }

    @Override
    public String getZnak() {
        return " - ";
    }

    @Override
    public int getPriorytet() {
        return 2;
    }
}
