package wyrazeniaarytmetyczne;

import static wyrazeniaarytmetyczne.FabrykaWyrazen.*;

public class Logarytm extends FunkcjaJednejZmiennej {

    public Logarytm(Wyrazenie w) {
        super(w);
    }

    // ln(x)' = 1/x
    @Override
    public Wyrazenie getWzorNaPochodna() {
        return podziel(FabrykaWyrazen.JEDEN, wyrazenie);
    }

    @Override
    public String toString() {
        return "ln(" + super.wyrazenie.toString() + ")";
    }

    @Override
    public double policzWartosc(double x) {
        return Math.log(wyrazenie.policzWartosc(x));
    }
}
