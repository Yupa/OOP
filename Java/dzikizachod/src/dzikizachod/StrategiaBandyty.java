package dzikizachod;

public abstract class StrategiaBandyty extends Strategia{
    
    @Override
    public boolean czyUzycDynamitu(Gra gra, Gracz rzucajacy) {
        return gra.getDystansPrawo(rzucajacy, gra.getSzeryf()) <= 3;
    }

}
