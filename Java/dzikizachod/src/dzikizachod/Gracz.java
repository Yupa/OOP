package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Gracz {
    protected int pozycja;
    protected List<Akcja> mojeAkcje;
    protected int zasieg;
    protected int maksymalneZycie;
    protected int obecneZycie;
    private boolean czyMaDynamit;
    protected Strategia mojaStrategia;
    protected Gra gra;
    protected List<Gracz> zabici;
    
    // Konstruktor bez strategii jest implementowany w podklasach
    Gracz(Strategia strategia) {
        mojeAkcje = new ArrayList<>();
        zabici = new ArrayList<>();
        mojaStrategia = strategia;
        Random r = new Random();
        maksymalneZycie = r.nextInt(2) + 3;
        obecneZycie = maksymalneZycie;
        zasieg = 1;
        czyMaDynamit = false;
    }
    
    public void dolaczDoGry(Gra gra, int pozycja) {
        this.pozycja = pozycja;
        this.gra = gra;
    }
    
    public int getObecneZycie() {
        return obecneZycie;
    }
    
    public boolean czyMartwy() {
        return (obecneZycie <= 0);
    }
    
    public int getPozycja() {
        return pozycja;
    }
    
    // Funkcja przeprowadzająca turę gracza
    public void wykonajRuch() {
        dobierzKarty();
        rozpatrzDynamit();
        gra.wypiszRuchy();
        // Jeśli gracz umarł po dynamicie to kończy ruch
        if(czyMartwy()) {
            gra.wypiszMartwyOdDynamitu();
            return;
        }
        leczenie();
        zwiekszenieZasiegu();
        strzal();
        // Jeśli po strzale gra powinna się skończyć to wychodzimy, inaczej rozpa
        if(gra.czyKoniecGry())
            return;
        dynamit();
    }
    
    // Dobieramy tyle kart ilu nam brakuje do pelnej reki
    private void dobierzKarty() {
        int brakujacychKart = Gra.MAKSYMALNA_ILOSC_KART_AKCJI - mojeAkcje.size();
        List<Akcja> noweAkcje = gra.getPulaAkcji().wylosujKarty(brakujacychKart);
        mojeAkcje.addAll(noweAkcje);
        gra.wypiszAkcje(mojeAkcje);
    }
    
    // Rozpatrujemy dynamit, jesli go mamy to rzucamy kostka
    private void rozpatrzDynamit() {
        if(!czyMaDynamit)
            return;
        Random r = new Random();
        // Imitacja rzutu kostką, dynamit wybucha z szansa 1/6
        if(r.nextInt(6) == 0) {
            gra.wypiszDynamitWybuchl();
            otrzymajObrazenia(gra.OBRAZENIA_OD_DYNAMITU);
        }
        else {
            gra.wypiszDynamitIdzie();
            gra.rzuconyDynamit(this);
        }
        czyMaDynamit = false;
    }
    
    // Funkcja odpowiadająca za używanie kart leczenia
    protected void leczenie() {
        int kartDoUsuniecia = 0;
        // Używamy tyle kart leczenia ile możemy
        for (Akcja akcja : mojeAkcje) {
            if(akcja == Akcja.ULECZ && obecneZycie < maksymalneZycie) {
                gra.wypiszUlecz();
                obecneZycie++;
                kartDoUsuniecia++;
            }
        }
        // Usuwamy użyte karty i przenosimy je na stos kart zużytych
        for (int i = 0; i < kartDoUsuniecia; i++) {
            mojeAkcje.remove(Akcja.ULECZ);
            gra.getPulaAkcji().zuzyj(Akcja.ULECZ);
        }
    }
    
    // Funkcja odpowiadająca za używanie kart zwiekszenia zasiegu
    private void zwiekszenieZasiegu() {
        // Używamy wszystkie zwiekszenia zasiegu jakie mamy
        int doUsunieciaZasiegJeden = 0;
        int doUsunieciaZasiegDwa = 0;
        for (Akcja akcja : mojeAkcje) {
            if(akcja == Akcja.ZASIEG_PLUS_JEDEN) {
                gra.wypiszZasiegPlusJeden();
                zasieg++;
                doUsunieciaZasiegJeden++;
            }
            else if(akcja == Akcja.ZASIEG_PLUS_DWA) {
                gra.wypiszZasiegPlusDwa();
                zasieg += 2;
                doUsunieciaZasiegDwa++;
            }
        }
        // Usuwamy użyte karty z ręki i przenosimy je na stos kart zużytych
        for (int i = 0; i < doUsunieciaZasiegJeden; i++) {
            mojeAkcje.remove(Akcja.ZASIEG_PLUS_JEDEN);
            gra.getPulaAkcji().zuzyj(Akcja.ZASIEG_PLUS_JEDEN);   
        }
        for (int i = 0; i < doUsunieciaZasiegDwa; i++) {
            mojeAkcje.remove(Akcja.ZASIEG_PLUS_DWA);
            gra.getPulaAkcji().zuzyj(Akcja.ZASIEG_PLUS_DWA);
        }
    }
    
    // Funkcja odpowiadająca za używanie kart strzalu
    private void strzal() {
        
        // Zliczamy ilość dostępnych strzałów
        int strzalow = 0;
        for(Akcja akcja : mojeAkcje) {
            if(akcja == Akcja.STRZEL) {
                strzalow++;
            }
        }
        // Strzelamy dopóki mamy karty i chcemy strzelać
        while(strzalow > 0) {
            List<Gracz> sasiedziPrawo = gra.getGraczePrawo(this, zasieg);
            List<Gracz> sasiedziLewo = gra.getGraczeLewo(this, zasieg);
            Gracz cel = mojaStrategia.wybierzCel(gra, this, sasiedziLewo, sasiedziPrawo, strzalow);
            // Jeśli nie chcemy strzelać to wychodzimy z pętli
            if(cel != null) {
                gra.strzal(this, cel);
                mojeAkcje.remove(Akcja.STRZEL);
                gra.getPulaAkcji().zuzyj(Akcja.STRZEL);
            }
            else {
                break;
            }
            strzalow--;
            // Jeśli po naszym strzale gra powinna się skończyć to wychodzimy
            if(gra.czyKoniecGry())
                break;
        }
    }
    
    // Funkcja odpowiadająca za używanie karty dynamitu
    private void dynamit() {
        boolean czyMaKarteDynamitu = false;
        for(Akcja akcja : mojeAkcje) {
            if(akcja == Akcja.DYNAMIT)
                czyMaKarteDynamitu = true;
        }
        if(czyMaKarteDynamitu && mojaStrategia.czyUzycDynamitu(gra, this)) {
            gra.wypiszDynamitUzyty();
            mojeAkcje.remove(Akcja.DYNAMIT);
            gra.rzuconyDynamit(this);
        }
    }
    
    // Funkcja mówiąca, że został nam przekazany dynamit
    public void otrzymanyDynamit() {
        this.czyMaDynamit = true;
    }
    
    // Jeśli zostaliśmy trafieni to odejmujemy sobie punkt życia
    public void trafiony(Gracz strzelec) {
        otrzymajObrazenia(gra.OBRAZENIA_OD_STRZALU);
        // Jeśli mieliśmy dynamit to przekazujemy go dalej
        if(czyMartwy() && czyMaDynamit)
            gra.rzuconyDynamit(this); 
    }
    
    private void otrzymajObrazenia(int ile) {
        obecneZycie -= ile;
        // Jeśli gracz umarł to zwraca karty
        if(obecneZycie <= 0) {
            zwrocKarty();
        }
    }
    
    private void zwrocKarty() {
        gra.getPulaAkcji().zwroc(mojeAkcje);
        mojeAkcje.clear();
    }
    
    // Jeśli nasz atak kogoś zabił to dodajemy go do naszej listy zabitych
    public void zabil(Gracz cel) {
        zabici.add(cel);
    }
    
    public boolean czyJestSzeryfem() {
        return false;
    }
    
    public boolean czyJestBandyta() {
        return false;
    }
    
    public boolean czyJestPomocnikiem() {
        return false;
    }
    
    public boolean czyZabilWiecejPomocnikowNizBandytow() {
        int zabitychPomocnikow = 0;
        int zabitychBandytow = 0;
        for (Gracz zabity : zabici) {
            if(zabity.czyJestBandyta())
                zabitychBandytow++;
            if(zabity.czyJestPomocnikiem())
                zabitychPomocnikow++;
        }
        return zabitychPomocnikow > zabitychBandytow;
    }
    
    // Funkcja przywracajaca gracza do stanu sprzed gry
    public void przywrocStan() {
        zwrocKarty();
        zasieg = 1;
        obecneZycie = maksymalneZycie;
        czyMaDynamit = false;
        gra = null;
        zabici.clear();
    }
}
