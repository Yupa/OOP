package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategiaSzeryfaDomyslna extends StrategiaSzeryfa {
    
    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Jeśli ktoś nas atakował to strzelamy do tej osoby
        List<Gracz> atakujacy = ((Szeryf)strzelec).getGraczeKtorzyDoMnieStrzelali();
        List<Gracz> doKogo = new ArrayList<>();
        for(Gracz gracz : sasiedziLewo) {
            if(atakujacy.contains(gracz))
                doKogo.add(gracz);
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(atakujacy.contains(gracz) && !doKogo.contains(gracz))
                doKogo.add(gracz);
        }
        // Jeśli nikt nas nie atakował to losujemy
        Random r = new Random();
        if(doKogo.isEmpty()) {
            doKogo.addAll(sasiedziLewo);
            for(Gracz gracz : sasiedziPrawo) {
                if(!doKogo.contains(gracz))
                    doKogo.add(gracz);
            }
        }
        return doKogo.get(r.nextInt(doKogo.size()));
    }
}
