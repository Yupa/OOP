package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategiaPomocnikaSzeryfaZliczajaca extends StrategiaPomocnikaSzeryfa {
    
    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Atakujemy osobę, która strzelała do szeryfa lub zabiła więcej pomocników niż bandytów
        // Nie atakujemy szeryfa, nawet jeśli zabił więcej pomocników niż bandytów
        List<Gracz> atakujacy = gra.getSzeryf().getGraczeKtorzyDoMnieStrzelali();
        List<Gracz> doKogo = new ArrayList<>();
        for(Gracz gracz : sasiedziLewo) {
            if(atakujacy.contains(gracz) || gracz.czyZabilWiecejPomocnikowNizBandytow())
                if(!gracz.czyJestSzeryfem())
                    doKogo.add(gracz);
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(atakujacy.contains(gracz) || gracz.czyZabilWiecejPomocnikowNizBandytow())
                if(!doKogo.contains(gracz) && !gracz.czyJestSzeryfem())
                    doKogo.add(gracz);
        }
        if(doKogo.isEmpty())
            return null;
        Random r = new Random();
        return doKogo.get(r.nextInt(doKogo.size()));
    }

}
