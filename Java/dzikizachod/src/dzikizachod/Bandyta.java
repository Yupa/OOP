package dzikizachod;

public class Bandyta extends Gracz {
    private int ostatniaTuraWKtorejZabilSwojego;
    
    Bandyta() {
        super(Strategia.STRATEGIA_BANDYTY_DOMYSLNA);
    }
    Bandyta(StrategiaBandyty strategia) {
        super(strategia);
    }
    
    @Override
    public boolean czyJestBandyta() {
        return true;
    }
    
    @Override
    public void zabil(Gracz zabity) {
        super.zabil(zabity);
        ostatniaTuraWKtorejZabilSwojego = gra.getObecnaTura();
    }
    
    @Override
    public void przywrocStan() {
        super.przywrocStan();
        ostatniaTuraWKtorejZabilSwojego = 0;
    }
    
    // Zwraca true jeśli zabił innego bandytę w tej turze
    public boolean czyZabilSwojegoWTejTurze() {
        return gra.getObecnaTura() == ostatniaTuraWKtorejZabilSwojego;
    }
}
