package dzikizachod;

public abstract class StrategiaSzeryfa extends Strategia {
    
    @Override
    public boolean czyUzycDynamitu(Gra gra, Gracz strzelec) {
        // Zakładamy, że szeryf nigdy nie chce użyć dynamitu
        return false;
    }
}
