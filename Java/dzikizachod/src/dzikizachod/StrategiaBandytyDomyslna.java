package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategiaBandytyDomyslna extends StrategiaBandyty {

    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Strzelamy w szeryfa jeśli możemy
        for(Gracz gracz : sasiedziLewo) {
            if(gracz.czyJestSzeryfem())
                return gracz;
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(gracz.czyJestSzeryfem())
                return gracz;
        }
        // Jeśli nie możemy strzelić w szeryfa to zadowalamy się dowolnym pomocnikiem
        List<Gracz> doKogo = new ArrayList<>();
        for(Gracz gracz : sasiedziLewo) {
            if(!gracz.czyJestBandyta())
                doKogo.add(gracz);
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(!gracz.czyJestBandyta() && !doKogo.contains(gracz))
                doKogo.add(gracz);
        }
        if(doKogo.isEmpty())
            return null;
        Random r = new Random();
        return doKogo.get(r.nextInt(doKogo.size()));
    }

}
