package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategiaSzeryfaZliczajaca extends StrategiaSzeryfa {

    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Strzelamy tylko do graczy podejrzanych o bycie bandytą
        List<Gracz> atakujacy = ((Szeryf)strzelec).getGraczeKtorzyDoMnieStrzelali();
        List<Gracz> doKogo = new ArrayList<>();
        for(Gracz gracz : sasiedziLewo) {
            if(atakujacy.contains(gracz) || gracz.czyZabilWiecejPomocnikowNizBandytow())
                doKogo.add(gracz);
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(atakujacy.contains(gracz) || gracz.czyZabilWiecejPomocnikowNizBandytow())
                if(!doKogo.contains(gracz))
                    doKogo.add(gracz);
        }
        if(doKogo.isEmpty())
            return null;
        Random r = new Random();
        return doKogo.get(r.nextInt(doKogo.size()));
    }
}
