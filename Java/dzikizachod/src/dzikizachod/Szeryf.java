package dzikizachod;

import java.util.ArrayList;
import java.util.List;

public class Szeryf extends Gracz {
    private List<Gracz> graczeKtorzyDoMnieStrzelali;
    
    Szeryf() {
        super(Strategia.STRATEGIA_SZERYFA_DOMYSLNA);
        super.maksymalneZycie = Gra.MAKSYMALNE_ZYCIE_SZERYFA;
        super.obecneZycie = super.maksymalneZycie;
        graczeKtorzyDoMnieStrzelali = new ArrayList<>();
    }
    Szeryf(StrategiaSzeryfa strategia) {
        super(strategia);
        super.maksymalneZycie = Gra.MAKSYMALNE_ZYCIE_SZERYFA;
        super.obecneZycie = super.maksymalneZycie;
        graczeKtorzyDoMnieStrzelali = new ArrayList<>();
    }
    
    public void uleczony() {
        super.obecneZycie++;
    }
    
    @Override
    public void trafiony(Gracz strzelec) {
        super.trafiony(strzelec);
        // Jeśli ktoś do nas strzelał to dodajemy go do listy
        if(!graczeKtorzyDoMnieStrzelali.contains(strzelec))
            graczeKtorzyDoMnieStrzelali.add(strzelec);
    }
    
    @Override
    public boolean czyJestSzeryfem() {
        return true;
    }
    
    public List<Gracz> getGraczeKtorzyDoMnieStrzelali() {
        return graczeKtorzyDoMnieStrzelali;
    }
    
    @Override
    public void przywrocStan() {
        super.przywrocStan();
        graczeKtorzyDoMnieStrzelali.clear();
    }
}
