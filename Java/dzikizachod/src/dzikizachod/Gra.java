package dzikizachod;

import java.util.ArrayList;
import java.util.List;

public class Gra {
    private static final int LICZBA_TUR_DO_REMISU = 42;
    public static final int MAKSYMALNE_ZYCIE_SZERYFA = 5;
    public static final int MAKSYMALNA_ILOSC_KART_AKCJI = 5;
    public static final int OBRAZENIA_OD_STRZALU = 1;
    public static final int OBRAZENIA_OD_DYNAMITU = 3;
    private PulaAkcji pulaAkcji;
    private List<Gracz> gracze;
    private Szeryf szeryf;
    private List<Gracz> bandyci;
    private int obecnaTura;
    
    public PulaAkcji getPulaAkcji() {
        return pulaAkcji;
    }
    
    public Szeryf getSzeryf() {
        return szeryf;
    }
    
    public int getObecnaTura() {
        return obecnaTura;
    }
    
    // Funkcja, ktora symuluje strzal
    public void strzal(Gracz strzelec, Gracz cel) {
        wypiszStrzal(cel.getPozycja() + 1);
        cel.trafiony(strzelec);
        if(cel.czyMartwy())
            strzelec.zabil(cel);
    }
    
    // Funkcja, ktora symuluje przekazanie dynamitu dalej lub uzycie go
    public void rzuconyDynamit(Gracz rzucajacy) {
        int nastepnyGracz = (rzucajacy.getPozycja() + 1) % gracze.size();
        while(nastepnyGracz != rzucajacy.getPozycja()) {
            // Przekazujemy dynamit najblizszemu zywemu graczowi
            if(!gracze.get(nastepnyGracz).czyMartwy()) {
                gracze.get(nastepnyGracz).otrzymanyDynamit();
                break;
            }
            nastepnyGracz++;
            nastepnyGracz %= gracze.size();
        }
    }
    
    public void uleczSzeryfa() {
        szeryf.uleczony();
        wypiszUlecz(szeryf.getPozycja() + 1);
    }
    
    // Zwraca listę graczy, którzy leżą w zasięgu na prawo od strzelającego
    public List<Gracz> getGraczePrawo(Gracz strzelec, int zasieg) {
        List<Gracz> graczeNaPrawo = new ArrayList<>();
        int obecnaPozycja = (strzelec.getPozycja() + 1) % gracze.size();
        while (zasieg > 0) {
            // Jeśli wszyscy gracze leżą w zasięgu to przerywamy
            if(strzelec.getPozycja() == obecnaPozycja)
                break;
            if(!gracze.get(obecnaPozycja).czyMartwy()) {
                graczeNaPrawo.add(gracze.get(obecnaPozycja));
                zasieg--;
            }
            obecnaPozycja++;
            obecnaPozycja %= gracze.size();
        }
        return graczeNaPrawo;
    }

    // Zwraca listę graczy, którzy leżą w zasięgu na lewo od strzelającego
    public List<Gracz> getGraczeLewo(Gracz strzelec, int zasieg) {
        List<Gracz> graczeLewo = new ArrayList<>();
        int wLewo = zasieg;
        int obecnaPozycja = (strzelec.getPozycja() + gracze.size() - 1) % gracze.size();
        while (wLewo > 0) {
            // Jeśli wszyscy gracze leżą w zasięgu to przerywamy
            if(strzelec.getPozycja() == obecnaPozycja)
                break;
            if(!gracze.get(obecnaPozycja).czyMartwy()) {
                graczeLewo.add(gracze.get(obecnaPozycja));
                wLewo--;
            }
            obecnaPozycja += gracze.size() - 1;
            obecnaPozycja %= gracze.size();   
        }
        return graczeLewo;
    }
    
    // Zwraca dystans od gracza pierwszego do drugiego idąc w prawo
    public int getDystansPrawo(Gracz pierwszy, Gracz drugi) {
        int dystans = 0;
        int pozycja = pierwszy.getPozycja();
        while(pozycja != drugi.getPozycja()) {
            pozycja++;
            if(pozycja == gracze.size())
                pozycja = 0;
            if(!gracze.get(pozycja).czyMartwy())
                dystans++;
        }
        return dystans;
    }
    
    // Zwraca dystans od gracza pierwszego do drugiego idąc w lewo
    public int getDystansLewo(Gracz pierwszy, Gracz drugi) {
        int dystans = 0;
        int pozycja = pierwszy.getPozycja();
        while(pozycja != drugi.getPozycja()) {
            if(pozycja == 0)
                pozycja = gracze.size();
            pozycja--;
            if(!gracze.get(pozycja).czyMartwy())
                dystans++;
        }
        return dystans;
    }
    
    public boolean czyKoniecGry() {
        return czySzeryfWygral() || czyBandyciWygrali();
    }
    private boolean czySzeryfWygral() {
        // Sprawdzamy czy wszyscy bandyci sa martwi
        boolean res = true;
        for(Gracz bandyta: bandyci)
            res = res && bandyta.czyMartwy();
        return res;
    }
    private boolean czyBandyciWygrali() {
        // Sprawdzamy czy szeryf jest martwy
        return szeryf == null || szeryf.czyMartwy();
    }
    
    private void koniecGry() {
        wypiszKoniec();
        if(czySzeryfWygral())
            wypiszWygranaSzeryfa();
        else if(czyBandyciWygrali())
            wypiszWygranaBandytow();
        else
            wypiszRemis();
        przywrocStan();
    }

    // Funkcja przywracająca graczy, pule akcji oraz obiekt gry do stanu przed rozgrywką
    private void przywrocStan() {
        for(Gracz g : gracze) {
            g.przywrocStan();
        }
        szeryf = null;
        bandyci.clear();
        pulaAkcji.przywrocStan();
        pulaAkcji = null;
        gracze = null;
        obecnaTura = 0;
    }
    
    public void rozgrywka(List<Gracz> gracze, PulaAkcji pulaAkcji) {
        wypiszStart();
        this.gracze = gracze;
        this.pulaAkcji = pulaAkcji;
        this.bandyci = new ArrayList<>();
        
        // Ustawianie pozycji graczy, znalezienie szeryfa i bandytow
        for (int i = 0; i < gracze.size(); i++) {
            gracze.get(i).dolaczDoGry(this, i);
            if(gracze.get(i).czyJestSzeryfem())
                szeryf = (Szeryf)gracze.get(i);
            if(gracze.get(i).czyJestBandyta())
                bandyci.add((Bandyta)gracze.get(i));
        }
        wypiszGraczy();
        for (obecnaTura = 1; obecnaTura <= LICZBA_TUR_DO_REMISU; obecnaTura++) {
            int obecnyGracz = szeryf.getPozycja();
            wypiszTure(obecnaTura);
            // Każdy gracz wykonuje swoją turę poczynając od szeryfa
            for (int i = 0; i < gracze.size(); i++) {
                wypiszGracza(obecnyGracz + 1);
                // Jeśli gracz nie żyje
                if(gracze.get(obecnyGracz).czyMartwy())
                    wypiszMartwy();
                else {
                    gracze.get(obecnyGracz).wykonajRuch();
                    wypiszGraczy();
                }
                obecnyGracz++;
                obecnyGracz %= gracze.size();
                // Sprawdzamy czy należy zakończyć grę
                if(czyKoniecGry()) {
                    koniecGry();
                    return;
                }
            }
        }
        koniecGry();
    }
    
    // Funkcje służące do wypisywania komunikatów
    private void wypiszGraczy() {
        System.out.println("\n  Gracze:");
        for(int i = 1; i <= gracze.size(); i++) {
            System.out.print("    " + i + ": ");
            if(gracze.get(i - 1).czyMartwy())
                System.out.print("X (");
            if(gracze.get(i - 1).czyJestSzeryfem())
                System.out.print("Szeryf");
            else if(gracze.get(i - 1).czyJestBandyta())
                System.out.print("Bandyta");
            else
                System.out.print("Pomocnik Szeryfa");
            if(gracze.get(i - 1).czyMartwy())
                System.out.println(")");
            else
                System.out.println(" (liczba żyć: " + gracze.get(i - 1).getObecneZycie() + ")");
        }
        System.out.println();
    }
    
    private void wypiszGracza(int ktoryGracz) {
        if(gracze.get(ktoryGracz - 1).czyJestSzeryfem())
            System.out.println("  GRACZ " + ktoryGracz + " (Szeryf)");
        else if(gracze.get(ktoryGracz - 1).czyJestBandyta())
            System.out.println("  GRACZ " + ktoryGracz + " (Bandyta)");
        else
            System.out.println("  GRACZ " + ktoryGracz + " (Pomocnik Szeryfa)");
    }
    
    private void wypiszMartwy() {
        System.out.println("    MARTWY\n");
    }
    
    public void wypiszMartwyOdDynamitu() {
        System.out.println("      MARTWY");
    }
    
    private void wypiszTure(int ktora) {
        System.out.println("** TURA " + ktora);
    }
    
    public void wypiszAkcje(List<Akcja> akcje) {
        System.out.print("    Akcje: [");
        int przecinkow = 0;
        for(Akcja akcja : akcje) {
            switch (akcja) {
                case ULECZ:
                    System.out.print("ULECZ");
                    break;
                case STRZEL:
                    System.out.print("STRZEL");
                    break;
                case ZASIEG_PLUS_JEDEN:
                    System.out.print("ZASIEG_PLUS_JEDEN");
                    break;
                case ZASIEG_PLUS_DWA:
                    System.out.print("ZASIEG_PLUS_DWA");
                    break;
                case DYNAMIT:
                    System.out.print("DYNAMIT");
                    break;
                default:
                    break;
            }
            if(przecinkow < akcje.size() - 1) {
                System.out.print(", ");
                przecinkow++;
            }
        }
        System.out.println("]");
    }
    
    private void wypiszStart() {
        System.out.println("** START");
    }
    private void wypiszKoniec() {
        System.out.println("**KONIEC");
    }
    
    public void wypiszDynamitWybuchl() {
        System.out.println("    Dynamit: WYBUCHŁ");
    }
    public void wypiszDynamitIdzie() {
        System.out.println("    Dynamit: PRZECHODZI DALEJ");
    }
    
    public void wypiszRuchy() {
        System.out.println("    Ruchy:");
    }
    
    public void wypiszUlecz() {
        System.out.println("      ULECZ");
    }
    public void wypiszUlecz(int pozycja) {
        System.out.println("      ULECZ " + pozycja );
    }
    public void wypiszZasiegPlusJeden() {
        System.out.println("      ZASIEG_PLUS_JEDEN");
    }
    public void wypiszZasiegPlusDwa() {
        System.out.println("      ZASIEG_PLUS_DWA");
    }
    public void wypiszStrzal(int pozycja) {
        System.out.println("      STRZEL " + pozycja);
    }
    public void wypiszDynamitUzyty() {
        System.out.println("      DYNAMIT");
    }

    public void wypiszWygranaSzeryfa() {
        System.out.println("  WYGRANA STRONA: szeryf i pomocnicy");
    }
    public void wypiszWygranaBandytow() {
        System.out.println("  WYGRANA STRONA: bandyci");
    }
    public void wypiszRemis() {
        System.out.println("  REMIS - OSIĄGNIĘTO LIMIT TUR");
    }
    
    
    public static void main(String[] args) {
        // Wersja z listą:
        List<Gracz> gracze = new ArrayList<Gracz>();
        gracze.add(new Szeryf());
        for(int i=0;i<2;i++) gracze.add(new PomocnikSzeryfa());
        for(int i=0;i<3;i++) gracze.add(new Bandyta());

        // Kod wspólny dla obu wersji:
        PulaAkcji pulaAkcji = new PulaAkcji();
        pulaAkcji.dodaj(Akcja.ULECZ, 20);
        pulaAkcji.dodaj(Akcja.STRZEL, 60);
        pulaAkcji.dodaj(Akcja.ZASIEG_PLUS_JEDEN, 3);
        pulaAkcji.dodaj(Akcja.ZASIEG_PLUS_DWA, 1);
        pulaAkcji.dodaj(Akcja.DYNAMIT, 1);

        Gra gra = new Gra();
        gra.rozgrywka(gracze, pulaAkcji);
    }
}
