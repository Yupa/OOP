package dzikizachod;

import java.util.List;

public class StrategiaBandytyCierpliwa extends StrategiaBandyty {
    
    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Jeśli nie możemy strzelić w szeryfa to nie strzelamy
        for (Gracz gracz : sasiedziLewo) {
            if(gracz.czyJestSzeryfem()) {
                return gracz;
            }
        }
        for (Gracz gracz : sasiedziPrawo) {
            if(gracz.czyJestSzeryfem()) {
                return gracz;
            }
        }
        return null;
    }
}
