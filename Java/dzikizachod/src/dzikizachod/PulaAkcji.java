package dzikizachod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PulaAkcji {
    
    private List<Akcja> pula;
    private List<Akcja> zuzyte;
    private List<Akcja> template;
    
    PulaAkcji() {
        pula = new ArrayList<>();
        zuzyte = new ArrayList<>();
        template = new ArrayList<>();
    }
    
    void dodaj(Akcja typ, int liczba) {
        for(int i = 0; i < liczba; i++) {
            pula.add(typ);
            template.add(typ);
        }
        // tasujemy przy dodawaniu
        Collections.shuffle(pula);
    }
    
    // Tasujemy zużyte karty i dokładamy do puli
    void tasuj() {
        pula.addAll(zuzyte);
        zuzyte.clear();
        Collections.shuffle(pula);
    }
    
    // Dodaje karte na stos kart zuzytych
    void zuzyj(Akcja karta) {
        zuzyte.add(karta);
    }
    
    // Zwraca karty do kart zuzytych
    void zwroc(List<Akcja> zwracane) {
        zuzyte.addAll(zwracane);
    }
    
    // Przywraca stan puli do tego, kiedy zostala stworzona
    public void przywrocStan() {
        zuzyte.clear();
        pula.clear();
        pula.addAll(template);
    }
    
    public List<Akcja> wylosujKarty(int liczba) {
        List<Akcja> wylosowane = new ArrayList<>();
        
        // Dopóki nie wylosowaliśmy tylu kart ilu wymagamy to losujemy i tasujemy
        while(wylosowane.size() < liczba) {
            // Dopóki pula kart nie jest pusta to zabieramy kartę z wierzchu
            while (wylosowane.size() < liczba && !pula.isEmpty()) {
                wylosowane.add(pula.remove(0));
            }
            // Jeśli karty się skończyły to dotasowujemy
            if(pula.isEmpty())
                tasuj();
        }
        return wylosowane;
    }
}
