package dzikizachod;

public class PomocnikSzeryfa extends Gracz {

    // Jeśli wywołany został konstruktor bez strategii to przekazujemy domyślną
    PomocnikSzeryfa() {
        super(Strategia.STRATEGIA_POMOCNIKA_DOMYSLNA);
    }
    PomocnikSzeryfa(StrategiaPomocnikaSzeryfa strategia) {
        super(strategia);
    }
    
    @Override
    public boolean czyJestPomocnikiem() {
        return true;
    }
    
    @Override
    protected void leczenie() {
        Szeryf szeryf = gra.getSzeryf();
        int doUsuniecia = 0;
        // Jeśli szeryf jest w zasiegu to leczymy najpierw jego
        if(gra.getDystansLewo(this, szeryf) == 1 || gra.getDystansPrawo(this, szeryf) == 1) {
            for (Akcja akcja : mojeAkcje) {
                if(akcja == Akcja.ULECZ) {
                    if (szeryf.getObecneZycie() < Gra.MAKSYMALNE_ZYCIE_SZERYFA) {
                        gra.uleczSzeryfa();
                        doUsuniecia++;
                    }
                }
            }
            // Odkładamy wykorzystane karty na stos kart zużytych
            for (int i = 0; i < doUsuniecia; i++) {
                mojeAkcje.remove(Akcja.ULECZ);
                gra.getPulaAkcji().zuzyj(Akcja.ULECZ);
            }
        }
        // Po uleczeniu szeryfa próbujemy leczyć siebie
        super.leczenie();
    }
    
    
}
