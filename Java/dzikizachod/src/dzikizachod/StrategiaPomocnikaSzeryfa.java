package dzikizachod;

import java.util.List;

public abstract class StrategiaPomocnikaSzeryfa extends Strategia {
    
    @Override
    public boolean czyUzycDynamitu(Gra gra, Gracz uzywajacy) {
        // Jeśli jesteśmy blisko szeryfa to nie używamy dynamitu
        if(gra.getDystansPrawo(uzywajacy, gra.getSzeryf()) <= 3)
            return false;
        // Zbieramy liste graczy do szeryfa
        List<Gracz> sasiedzi = gra.getGraczePrawo(uzywajacy, 
                                gra.getDystansPrawo(uzywajacy, gra.getSzeryf()) - 1);
        List<Gracz> atakujacy = gra.getSzeryf().getGraczeKtorzyDoMnieStrzelali();
        int ilePodejrzanych = 0;
        
        // Zliczamy podejrzanych
        for(Gracz gracz : sasiedzi) {
            if(gracz.czyZabilWiecejPomocnikowNizBandytow() || atakujacy.contains(gracz))
                ilePodejrzanych++;
        }
        // Sprawdzamy czy podejrzani stanowia co najmniej 2/3 graczy od nas do szeryfa
        if(ilePodejrzanych * 3 >= 2 * sasiedzi.size())
            return true;
        return false;
    }
}
