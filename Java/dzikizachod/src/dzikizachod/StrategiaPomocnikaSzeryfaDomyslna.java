package dzikizachod;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StrategiaPomocnikaSzeryfaDomyslna extends StrategiaPomocnikaSzeryfa {
    
    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Strzelamy do losowej osoby nie bedącej szeryfem
        List<Gracz> doKogo = new ArrayList<>();
        for(Gracz gracz : sasiedziLewo) {
            if(!gracz.czyJestSzeryfem())
                doKogo.add(gracz);
        }
        for(Gracz gracz : sasiedziPrawo) {
            if(!gracz.czyJestSzeryfem() && !doKogo.contains(gracz))
                doKogo.add(gracz);
        }
        if(doKogo.isEmpty())
            return null;
        Random r = new Random();
        return doKogo.get(r.nextInt(doKogo.size()));
    }
}
