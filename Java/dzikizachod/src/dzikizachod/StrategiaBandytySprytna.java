package dzikizachod;

import java.util.List;

public class StrategiaBandytySprytna extends StrategiaBandyty {
    
    @Override
    public Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                            List<Gracz> sasiedziPrawo, int strzalow) {
        // Jeśli możemy strzelić w szeryfa to strzelamy
        for (Gracz gracz : sasiedziLewo) {
            if(gracz.czyJestSzeryfem()) {
                return gracz;
            }
        }
        for (Gracz gracz : sasiedziPrawo) {
            if(gracz.czyJestSzeryfem()) {
                return gracz;
            }
        }
        // Jeśli nie ma szeryfa to próbujemy zabić swojego
        if(((Bandyta)strzelec).czyZabilSwojegoWTejTurze() == false) {
            // Jeśli nie to próbujemy zabić swojego
            for(Gracz gracz : sasiedziLewo) {
                if(gracz.czyJestBandyta() && gracz.getObecneZycie() <= strzalow) {
                    return gracz;
                }
            }
            for(Gracz gracz : sasiedziPrawo) {
                if(gracz.czyJestBandyta() && gracz.getObecneZycie() <= strzalow) {
                    return gracz;
                }
            }
        }
        // Inaczej używamy strategii domyślnej    \
        return Strategia.STRATEGIA_BANDYTY_DOMYSLNA.wybierzCel(gra, strzelec, sasiedziLewo, 
                                                                sasiedziPrawo, strzalow);
    }
}
