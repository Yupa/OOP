package dzikizachod;

import java.util.List;

public abstract class Strategia {
    
    public static StrategiaSzeryfa STRATEGIA_SZERYFA_DOMYSLNA =  
            new StrategiaSzeryfaDomyslna();
    public static StrategiaPomocnikaSzeryfa STRATEGIA_POMOCNIKA_DOMYSLNA =  
            new StrategiaPomocnikaSzeryfaDomyslna();
    public static StrategiaBandyty STRATEGIA_BANDYTY_DOMYSLNA =  
            new StrategiaBandytyDomyslna();
    
    public abstract Gracz wybierzCel(Gra gra, Gracz strzelec, List<Gracz> sasiedziLewo, 
                                    List<Gracz> sasiedziPrawo, int strzalow);
    
    public abstract boolean czyUzycDynamitu(Gra gra, Gracz rzucajacy);
    
}
