/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package znajdzcykl;

/**
 *
 * @author rp386037
 */
public class ZnajdzCykl {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static long znajdzCyklOdwracanie(int[] t) {
        int iter = t[0], iter2 = 0;
        int krokow = 1;
        while(iter > 0) {
            iter = t[iter];
            t[t[iter2]] = iter2;
            iter2 = t[iter2];
            krokow++;
        }
        if (iter == -1)
            return -1;
        long dlugoscCyklu = 1, dlugoscOgona = 0;
        iter = iter2 = 0;
        for (int i = 0; i < krokow / 2; ++i)
            iter = t[iter];
        iter2 = iter;
        while(t[iter] != iter2) {
            ++dlugoscCyklu;
            iter = t[iter];
        }
        dlugoscOgona = (krokow - dlugoscCyklu) / 2;
        return dlugoscCyklu;
    }
    
    public static long znajdzDlugoscCykluZolw(int[] t) {
        int zolw = 0;
        int achilles = 0;
        boolean coDrugi = false;
        while(achilles != -1 && achilles != zolw) {
            if (coDrugi) {
                zolw = t[zolw];
                coDrugi = false;
            }
            else coDrugi = true;
            achilles = t[achilles];
        }
        if(achilles == -1)
            return -1;
        
        long dlugoscCyklu = 1, dlugoscOgona = 0;
        while (t[achilles] != zolw) {
            dlugoscCyklu++;
            achilles = t[achilles];
        }
        zolw = achilles = 0;
        for (int i = 0; i < dlugoscCyklu; ++i)
            achilles = t[achilles];
        while (zolw != achilles) {
            dlugoscOgona++;
            zolw = t[zolw];
            achilles = t[achilles];
        }
        return dlugoscCyklu;
    }
    
}
