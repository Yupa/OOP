package swing;

import java.awt.*;
import javax.swing.*;

public class DFlowLayoutTest extends JFrame {

    DFlowLayoutTest() {
        super("Okno FlowLayoutTest");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new FlowLayout());
        add(new JButton("Psk."));
        add(new JButton("Przycisk"));
        add(new JButton("Długi przycisk"));
        add(new JButton("B. długi przycisk"));
        add(new JButton("Bardzo długi przycisk"));
        add(new JButton("Bardzo bardzo długi przycisk"));
        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new DFlowLayoutTest();
            }
        });
    }
}
