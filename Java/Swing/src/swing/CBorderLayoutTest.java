package swing;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;

public class CBorderLayoutTest extends JFrame {

    CBorderLayoutTest() {
        super("Okno BorderLayoutTest");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Główne kontenery (JApplet, JDialog i JFrame) domyślnie używają BorderLayout.
        //Natomiast obiekty JPanel domyślnie używają FlowLayout.
        add(new JButton("CENTER")); //domyślna pozycja to BorderLayout.CENTER
        add(BorderLayout.NORTH, new JButton("NORTH"));
        add(BorderLayout.SOUTH, new JButton("SOUTH"));
        add(BorderLayout.EAST, new JButton("EAST"));
        add(BorderLayout.WEST, new JButton("WEST"));

        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new CBorderLayoutTest();
            }
        });
    }
}
