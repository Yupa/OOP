package swing;

import java.awt.*;
import java.util.*;
import java.util.List;
import javax.swing.*;
import javax.swing.event.ListDataListener;

class MyComboModel implements ComboBoxModel {

    private List data = new ArrayList();
    private int selected = 0;

    public MyComboModel(List list) {
        data = list;
    }

    public void setSelectedItem(Object o) {
        selected = data.indexOf(o);
    }

    public Object getSelectedItem() {
        return data.get(selected);
    }

    public int getSize() {
        return data.size();
    }

    public Object getElementAt(int i) {
        return data.get(i);
    }

    public void addListDataListener(ListDataListener l) {
        //rejestruje listenery zainteresowane zmianami modelu
    }

    public void removeListDataListener(ListDataListener l) {
        //usuwa listenery zainteresowane zmianami modelu
    }
}

public class GComboBoxModel extends JFrame {

    GComboBoxModel() {
        super("Okno ComboBoxModel");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String[] data = {"Ala", "Ola", "Ela", "Ula"};
        MyComboModel model = new MyComboModel(java.util.Arrays.asList(data));

        JComboBox cb1 = new JComboBox();
        JComboBox cb2 = new JComboBox();
        cb1.setModel(model);
        cb2.setModel(model);

        setLayout(new FlowLayout());

        add(cb1);
        add(cb2);
        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new GComboBoxModel();
            }
        });
    }
}
