package swing;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                //AWitajSwiecie.utwórzGUI();
                //new BWitajSwiecie();//uwaga: trzeba ręcznie zabijać proces!
                //new CBorderLayoutTest();
                //new DFlowLayoutTest();
                //new EGridLayoutTest();
                //new FZliczanieKlikniec();
                new GComboBoxModel();
            }
        });
    }
}
