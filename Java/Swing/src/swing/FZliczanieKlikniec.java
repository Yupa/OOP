package swing;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FZliczanieKlikniec extends JFrame {

    Integer licznikKliknięć = 0;
    JLabel etykieta;

    class ZwiększanieLicznika implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            //obiekt klasy wewnętrznej ma dostęp do składowych obiektu klasy otaczającej
            licznikKliknięć++;
            etykieta.setText("Dotychczas kliknąłeś " +
                    licznikKliknięć +
                    (licznikKliknięć == 1 ? " raz" : " razy"));
        }
    }

    FZliczanieKlikniec() {
        super("Okno ZliczanieKliknięć");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 0));

        JButton przycisk = new JButton("Kliknij");
        przycisk.addActionListener(new ZwiększanieLicznika());
        panel.add(przycisk);

        etykieta = new JLabel("Jeszcze nie kliknięto ani razu");
        panel.add(new JPanel());//pusty panel tworzy odstęp
        panel.add(etykieta);
        //puste obramowanie odsuwa komponenty od krawędzi
        panel.setBorder(BorderFactory.createEmptyBorder(30, 60, 10, 60));
        add(panel);

        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new FZliczanieKlikniec();
            }
        });
    }
}
