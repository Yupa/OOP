package swing;

import java.awt.GridLayout;
import javax.swing.*;

public class EGridLayoutTest extends JFrame {

    EGridLayoutTest() {
        super("Okno GridLayoutTest");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setLayout(new GridLayout(3, 2));
        add(new JButton("P1"));
        add(new JButton("P2"));
        add(new JButton("P3"));
        add(new JButton("P4"));
        add(new JButton("Bardzo długi przycisk"));
        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new EGridLayoutTest();
            }
        });
    }
}
