package swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Dodawanie extends JFrame {

    JFrame okno = this;
    JTextField wynik = new JTextField(9);
    JTextField pole1 = new JTextField(9);
    JTextField pole2 = new JTextField(9);
    //do obsługi zdarzeń często używane są anonimowe klasy wewnętrzne
    //jeden egzemplarz będzie dzielony przez oba pola
    ActionListener sumowanie = new ActionListener() {

        public void actionPerformed(ActionEvent ev) {
            try {
                Integer w = Integer.parseInt(pole1.getText()) +
                        Integer.parseInt(pole2.getText());
                wynik.setText(w.toString());
            } catch (NumberFormatException ex) {
                wynik.setText("Błąd");
            }
        }
    };

    Dodawanie() {
        super("Okno Dodawanie");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());

        pole1.addActionListener(sumowanie);
        panel.add(pole1);
        panel.add(new JLabel("+"));
        pole2.addActionListener(sumowanie);
        panel.add(pole2);
        panel.add(new JLabel("="));
        wynik.setEditable(false);
        panel.add(wynik);

        //puste obramowanie odsuwa komponenty od krawędzi
        panel.setBorder(BorderFactory.createEmptyBorder(30, 30, 30, 30));
        add(panel);

        setSize(300, 150);
        setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń, tworzenie GUI zawsze zlecamy dla wątku obsługi zdarzeń
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                new Dodawanie();
            }
        });
    }
}
