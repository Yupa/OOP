package swing;

import javax.swing.*;

public class AWitajSwiecie {

    static void utwórzGUI() {
        //tworzenie nowego okna
        JFrame frame = new JFrame("Okno WitajSwiecie");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //dodawanie etykiety z przywitaniem
        JLabel label = new JLabel("Witaj świecie!");
        frame.add(label);

        //ustalanie wymiarów i wyświetlanie okna
        //frame.pack(); //względem komponentów
        frame.setSize(300, 150);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        //aby uniknąć zakleszczeń tworzenie GUI
        //zlecamy do wątku obsługi zdarzeń
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                utwórzGUI();
            }
        });
    }
}