

public enum Znak {
    RAZY, MINUS, PLUS;
    
    public static Znak rozczytaj(char c) {
        if(c == 'x')
            return Znak.RAZY;
        else if(c == '-')
            return Znak.MINUS;
        else
            return Znak.PLUS;
    }
}
