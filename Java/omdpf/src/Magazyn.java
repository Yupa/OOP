import java.beans.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Magazyn implements Serializable {
    
    public static final String LISTA_FARB_PROPERTY = "listaFarb";
    public static final String LISTA_PIGMENTOW_PROPERTY = "listaPigmentow";
    public static final String LISTA_REGUL_PROPERTY = "listaRegul";
    public static final String OBECNA_FARBA_PROPERTY = "obecnaFarba";
    public static final String OBECNY_PIGMENT_PROPERTY = "obecnyPigment";
    public static final String OBECNA_REGULA_PROPERTY = "obecnaRegula";
    
    private List<Farba> listaFarb;
    private List<Pigment> listaPigmentow;
    private List<Regula> listaRegul;
    private Farba obecnaFarba;
    private Pigment obecnyPigment;
    private Regula obecnaRegula;
    private PropertyChangeSupport listaFarbSupport;
    private PropertyChangeSupport listaPigmentowSupport;
    private PropertyChangeSupport listaRegulSupport;
    private PropertyChangeSupport obecnaFarbaSupport;
    private PropertyChangeSupport obecnyPigmentSupport;
    private PropertyChangeSupport obecnaRegulaSupport;
    
    private Random r;
    
    public Magazyn() {
        listaFarbSupport = new PropertyChangeSupport(this);
        listaPigmentowSupport = new PropertyChangeSupport(this);
        listaRegulSupport = new PropertyChangeSupport(this);
        obecnaFarbaSupport = new PropertyChangeSupport(this);
        obecnyPigmentSupport = new PropertyChangeSupport(this);
        obecnaRegulaSupport = new PropertyChangeSupport(this);
        listaFarb = new ArrayList<>();
        listaPigmentow = new ArrayList<>();
        listaRegul = new ArrayList<>();
        obecnyPigment = new Pigment("", "+", 0, "+", 0);
        obecnaFarba = new Farba("", 0, 0);
        obecnaRegula = new Regula("", "");
        r = new Random();
    }
    
    public List<Farba> getListaFarb() {
        return listaFarb;
    }
    
    public void setListaFarb(List<Farba> value) {
        listaFarb = value;
        listaFarbSupport.firePropertyChange(LISTA_FARB_PROPERTY, null, value);
    }
    
    public void addListaFarbChangeListener(PropertyChangeListener listener) {
        listaFarbSupport.addPropertyChangeListener(listener);
    }
    
    public void removeListaFarbChangeListener(PropertyChangeListener listener) {
        listaFarbSupport.removePropertyChangeListener(listener);
    }
    
    public List<Pigment> getListaPigmentow() {
        return listaPigmentow;
    }

    public void setListaPigmentow(List<Pigment> value) {
        listaPigmentow = value;
        listaPigmentowSupport.firePropertyChange(LISTA_PIGMENTOW_PROPERTY, null, value);
    }
    
    public void addListaPigmentowChangeListener(PropertyChangeListener listener) {
        listaPigmentowSupport.addPropertyChangeListener(listener);
    }
    
    public void removeListaPigmentowChangeListener(PropertyChangeListener listener) {
        listaPigmentowSupport.removePropertyChangeListener(listener);
    }
    
    public Farba getObecnaFarba() {
        return obecnaFarba;
    }

    public void setObecnaFarba(Farba value) {
        obecnaFarba = value;
        obecnaFarbaSupport.firePropertyChange(OBECNA_FARBA_PROPERTY, null, obecnaFarba);
    }
    
    public void addObecnaFarbaChangeListener(PropertyChangeListener listener) {
        obecnaFarbaSupport.addPropertyChangeListener(listener);
    }
    
    public void removeObecnaFarbaChangeListener(PropertyChangeListener listener) {
        obecnaFarbaSupport.removePropertyChangeListener(listener);
    }

    public Pigment getObecnyPigment() {
        return obecnyPigment;
    }

    public void setObecnyPigment(Pigment value) {
        obecnyPigment = value;
        setListaRegul(obecnyPigment.getReguly());
        obecnyPigmentSupport.firePropertyChange(OBECNY_PIGMENT_PROPERTY, null, obecnyPigment);
    }
    
    public void addObecnyPigmentChangeListener(PropertyChangeListener listener) {
        obecnyPigmentSupport.addPropertyChangeListener(listener);
    }
    
    public void removeObecnyPigmentChangeListener(PropertyChangeListener listener) {
        obecnyPigmentSupport.removePropertyChangeListener(listener);
    }
    
    public List<Regula> getListaRegul() {
        return listaRegul;
    }
    
    public void setListaRegul(List<Regula> value) {
        listaRegul = value;
        listaRegulSupport.firePropertyChange(LISTA_REGUL_PROPERTY, null, value);
    }
    
    public void addListaRegulChangeListener(PropertyChangeListener listener) {
        listaRegulSupport.addPropertyChangeListener(listener);
    }
    
    public void removeListaRegulChangeListener(PropertyChangeListener listener) {
        listaRegulSupport.removePropertyChangeListener(listener);
    }
    
    public Regula getObecnaRegula() {
        return obecnaRegula;
    }

    public void setObecnaRegula(Regula value) {
        obecnaRegula = value;
        obecnaRegulaSupport.firePropertyChange(OBECNA_REGULA_PROPERTY, null, obecnaRegula);
    }
    
    public void addObecnaRegulaChangeListener(PropertyChangeListener listener) {
        obecnaRegulaSupport.addPropertyChangeListener(listener);
    }
    
    public void removeObecnaRegulaChangeListener(PropertyChangeListener listener) {
        obecnaRegulaSupport.removePropertyChangeListener(listener);
    }

    public boolean dodajFarbe(Farba f) {
        listaFarb.add(f);
        setListaFarb(listaFarb);
        return true;
    }
    
    public boolean dodajPigment(Pigment p) {
        listaPigmentow.add(p);
        setListaPigmentow(listaPigmentow);
        return true;
    }
    
    public boolean dodajRegule(Pigment p) {
        Regula nowaRegula = new Regula("", "");
        p.dodajRegule(nowaRegula);
        setObecnyPigment(obecnyPigment);
        setObecnaRegula(nowaRegula);
        return true;
    }
    
    private String losujNazwe() {
        int dlugosc = r.nextInt(7)+4;
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < dlugosc; i++) {
            char c = chars[r.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }
    
    private String losujZnak() {
    	int jakiZnak = r.nextInt(3);
        switch (jakiZnak) {
            case 0:
                return "+";
            case 1:
                return "-";
            default:
                return "x";
        }
    }
    
    public Pigment stworzLosowyPigment() {
        String nazwa = losujNazwe();
        String znakToksycznosci = losujZnak();
        String znakJakosci = losujZnak();
        double toksycznosc = r.nextDouble()*100;
        double jakosc = r.nextDouble()*100;
        System.out.println(znakToksycznosci + " , " + znakJakosci);
        Pigment wynik = new Pigment(nazwa, znakToksycznosci, toksycznosc, znakJakosci, jakosc);
        dodajRegule(wynik);
        return wynik;
    }
    
    public Farba stworzLosowaFarbe() {
        Random r = new Random();
        String nazwa = losujNazwe();
        double toksycznosc = r.nextInt(101);
        double jakosc = r.nextInt(101);
        return new Farba(nazwa, toksycznosc, jakosc);
    }
}
