

public class Farba {
    private String kolor;
    private double toksycznosc;
    private double jakosc;
    
    public Farba(String kolor, double toksycznosc, double jakosc) {
        this.kolor = kolor;
        this.toksycznosc = toksycznosc;
        this.jakosc = jakosc;
    }
    
    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public double getToksycznosc() {
        return toksycznosc;
    }

    public void setToksycznosc(double toksycznosc) {
        this.toksycznosc = toksycznosc;
    }

    public double getJakosc() {
        return jakosc;
    }

    public void setJakosc(double jakosc) {
        this.jakosc = jakosc;
    }
    
    @Override
    public String toString() {
        return kolor;
    }
    
    public String wypisz() {
        return "Kolor: " + kolor + ", jakosc: " + jakosc + ", toksycznosc: " + toksycznosc;
    }
}
