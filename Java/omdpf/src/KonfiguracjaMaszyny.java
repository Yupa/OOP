
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class KonfiguracjaMaszyny {
    Maszyna maszyna;
    private String farbyFile;
    private String pigmentyFile;
    private static final String CONFIGURATION_FILE = "maszyna.conf";

    public Maszyna getMaszyna() {
        return maszyna;
    }
    
    public boolean wczytaj() {
        String fileName = CONFIGURATION_FILE;

        // This will reference one line at a time
        String line = null;

        try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            if((line = bufferedReader.readLine()) != null)
                farbyFile = line;
            else
                throw new IOException();
            if((line = bufferedReader.readLine()) != null)
                pigmentyFile = line;
            else
                throw new IOException();
            
            
            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + fileName + "'");     
            return false;
        }
        catch(IOException ex) {
            System.out.println("Error reading file '" + fileName + "'");
            return false;
        }
        
        // Tutaj wczytywalibyśmy maszynę
        maszyna = new SymulacjaMaszyny();
        return true;
    }
    
    private boolean wczytajFarby(Magazyn magazyn) {
        //try with resources zapewnia nam zwolnienie zasobow pliku
        try (Scanner scan = new Scanner(new File(farbyFile))){
            while(scan.hasNextLine()) {
                String s = scan.nextLine();
                try (Scanner scan2 = new Scanner(s).useLocale(Locale.US)) {
                    String kolor = scan2.next();
                    Double toksycznosc = scan2.nextDouble();
                    Double jakosc = scan2.nextDouble();
                    if(!magazyn.dodajFarbe(new Farba(kolor, toksycznosc, jakosc)))
                        return false;
                }
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku");
            return false;
        }
        catch (IllegalStateException e) {
            System.out.println("Scanner został zamknięty");
            return false;
        }
        catch (InputMismatchException e) {
            System.out.println("Błędne dane wejściowe");
            return false;
        }
        catch (NoSuchElementException e) {
            System.out.println("Koniec pliku");
            return false;
        }
        return true;
    }
    
    private boolean wczytajPigmenty(Magazyn magazyn) {
        //try with resources zapewnia nam zwolnienie zasobow pliku
        try (Scanner scan = new Scanner(new File(pigmentyFile))){
            while(scan.hasNextLine()) {
                String s = scan.nextLine();
                try (Scanner scan2 = new Scanner(s).useLocale(Locale.US)) {
                    String nazwa = scan2.next();
                    String jakiKolor = scan2.next();
                    String naJakiKolor = scan2.next();
                    //System.out.println(nazwa + ", " + jakiKolor + ", " + naJakiKolor);
                    char jakiZnakToksycznosc = scan2.findInLine(" .").charAt(1);
                    //System.out.println(nazwa + ", " + jakiKolor + ", " + naJakiKolor + ", " + jakiZnakToksycznosc);
                    Double toksycznosc = scan2.nextDouble();
                    //System.out.println(nazwa + ", " + jakiKolor + ", " + naJakiKolor + ", " + jakiZnakToksycznosc + toksycznosc);
                    char jakiZnakJakosc = scan2.findInLine(" .").charAt(1);
                    Double jakosc = scan2.nextDouble();
                    Pigment p = new Pigment(nazwa, "" + jakiZnakToksycznosc, toksycznosc, "" + jakiZnakJakosc, jakosc);
                    p.dodajRegule(new Regula(jakiKolor, naJakiKolor));
                    if(!magazyn.dodajPigment(p))
                        return false;
                }
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku");
            return false;
        }
        catch (IllegalStateException e) {
            System.out.println("Scanner został zamknięty");
            return false;
        }
        catch (InputMismatchException e) {
            System.out.println("Błędne dane wejściowe");
            return false;
        }
        catch (NoSuchElementException e) {
            System.out.println("Koniec pliku");
            return false;
        }
        return true;
    }
    
    public boolean wczytajFarbyIPigmenty(Magazyn magazyn) {
        return (wczytajFarby(magazyn) && wczytajPigmenty(magazyn));
    }
}
