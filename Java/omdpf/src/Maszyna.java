

import java.util.List;

public interface Maszyna {
    public void mieszaj(Farba farba);
    public boolean uzyjPigmentu(Farba farba, Pigment pigment, List<Regula> reguly);
    public void zakonczMieszanie();
}
