


public class Regula {
    private String farbaWejsciowa;
    private String farbaWyjsciowa;

    public Regula(String farbaWejsciowa, String farbaWyjsciowa) {
        this.farbaWejsciowa = farbaWejsciowa;
        this.farbaWyjsciowa = farbaWyjsciowa;
    }
    

    public String getFarbaWejsciowa() {
        return farbaWejsciowa;
    }

    public void setFarbaWejsciowa(String farbaWejsciowa) {
        this.farbaWejsciowa = farbaWejsciowa;
    }

    public String getFarbaWyjsciowa() {
        return farbaWyjsciowa;
    }

    public void setFarbaWyjsciowa(String farbaWyjsciowa) {
        this.farbaWyjsciowa = farbaWyjsciowa;
    }
    
    @Override
    public String toString() {
        return farbaWejsciowa + " -> " + farbaWyjsciowa;
    }
}
