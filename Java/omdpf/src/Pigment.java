

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Pigment {
    private String nazwa;
    private String znakToksycznosc;
    private double toksycznosc;
    private String znakJakosc;
    private double jakosc;
    private List<Regula> reguly;

    public Pigment(String nazwa, String znakToksycznosc, double toksycznosc, String znakJakosc, double jakosc) {
        this.nazwa = nazwa;
        this.znakToksycznosc = znakToksycznosc;
        this.toksycznosc = toksycznosc;
        this.znakJakosc = znakJakosc;
        this.jakosc = jakosc;
        reguly = new ArrayList<Regula>();
    }
    
    public List<Regula> getReguly() {
        return reguly;
    }

    public void setReguly(List<Regula> reguly) {
        this.reguly = reguly;
    }

    public String getZnakToksycznosc() {
        return znakToksycznosc;
    }

    public void setZnakToksycznosc(String znakToksycznosc) {
        this.znakToksycznosc = znakToksycznosc;
    }

    public String getZnakJakosc() {
        return znakJakosc;
    }

    public void setZnakJakosc(String znakJakosc) {
        this.znakJakosc = znakJakosc;
    }
    
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getStringToksycznosc() {
        return znakToksycznosc;
    }

    public void setStringToksycznosc(String znakToksycznosc) {
        this.znakToksycznosc = znakToksycznosc;
    }

    public double getToksycznosc() {
        return toksycznosc;
    }

    public void setToksycznosc(double toksycznosc) {
        this.toksycznosc = toksycznosc;
    }

    public String getStringJakosc() {
        return znakJakosc;
    }

    public void setStringJakosc(String znakJakosc) {
        this.znakJakosc = znakJakosc;
    }

    public double getJakosc() {
        return jakosc;
    }

    public void setJakosc(double jakosc) {
        this.jakosc = jakosc;
    }
    
    
    public String wypisz() {
        return "Nazwa: " + nazwa + ", jakosc: " + znakJakosc + jakosc + ", toksycznosc: " + znakToksycznosc + toksycznosc;
    }
    
    public void dodajRegule(Regula r) {
        reguly.add(r);
    }
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pigment other = (Pigment) obj;
        if (!Objects.equals(this.nazwa, other.nazwa)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return nazwa;
    }
}
