

import static java.lang.Double.max;
import static java.lang.Double.min;
import java.util.List;

public class SymulacjaMaszyny implements Maszyna {
    
    public void mieszaj(Farba farba) {
        System.out.println("Zaczynam mieszanie. Farba: " + farba.wypisz());
    }
    
    private double oblicz(double wejscie, String znak, double zmiana) {
        if(znak.equals("+"))
            return min(wejscie + zmiana, 100);
        else if(znak.equals("-"))
            return max(wejscie - zmiana, 0);
        else if(znak.equals("x"))
            return min(wejscie * zmiana, 100);
        else {
            System.out.println("BŁĘDNY ZNAK: " + znak);
            return 0;
        }
    }
    
    public boolean uzyjPigmentu(Farba farba, Pigment pigment, List<Regula> reguly) {
        Regula wybranaRegula = null;
        for(Regula r : reguly) {
            if(r.getFarbaWejsciowa().equals(farba.getKolor())) {
                wybranaRegula = r;
                break;
            }
        }
        if(wybranaRegula == null)
            return false;
        farba.setKolor(wybranaRegula.getFarbaWyjsciowa());
        farba.setJakosc(oblicz(farba.getJakosc(), pigment.getZnakJakosc(), pigment.getJakosc()));
        farba.setToksycznosc(oblicz(farba.getToksycznosc(), pigment.getZnakToksycznosc(), pigment.getToksycznosc()));
        System.out.println("Dodaję pigment: " + pigment.wypisz());
        System.out.println("Otrzymałem farbę: " + farba.wypisz());
        return true;
    }
    public void zakonczMieszanie() {
        System.out.println("Koniec mieszania");    
    }
}
