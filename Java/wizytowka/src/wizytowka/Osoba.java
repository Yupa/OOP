package wizytowka;

import java.io.Serializable;
import java.util.Random;

public class Osoba implements Serializable {
    
    private String imie;
    private String nazwisko;
    private String wiek;
    private String plec;
    private String pesel;
    private final Random random;
    
    public Osoba() {
        imie = "";
        nazwisko = "";
        wiek = "";
        plec = "";
        pesel = "";
        random = new Random();
    }
    
    // funkcja losująca wiek
    private void losujWiek() {
        wiek = "" + random.nextInt(120);
    }
    
    // funkcja losująca pesel - 11 cyfr
    private void losujPesel() {
        String s = "";
        for (int i = 0; i < 11; i++)
            s += random.nextInt(10);
        pesel = s;
    }
    
    // funkcja zmieniająca jedną literę w imieniu
    private void losujImie() {
        if(nazwisko.length() == 0)
            return;
        int zmien = random.nextInt(imie.length());
        String s = "";
        for(int i = 0; i < imie.length(); i++) {
            if(i == zmien)
                s += (char)(random.nextInt(26) + 'a');
            else
                s += imie.charAt(i);
        }
        imie = s;
    }
    
    // funkcja zmieniająca jedną literę w nazwisku
    private void losujNazwisko() {
        if(nazwisko.length() == 0)
            return;
        int zmien = random.nextInt(nazwisko.length());
        String s = "";
        for(int i = 0; i < nazwisko.length(); i++) {
            if(i == zmien)
                s += (char)(random.nextInt(26) + 'a');
            else
                s += nazwisko.charAt(i);
        }
        nazwisko = s;
    }
    
    // funkcja losująca płeć
    private void losujPlec() {
        if(random.nextInt(2) == 0)
            plec = "Mężczyzna";
        else
            plec = "Kobieta";
    }
    
    // funkcja losująca dane osoby
    public void losuj () {
        losujImie();
        losujNazwisko();
        losujWiek();
        losujPesel();
        losujPlec();
    }
    
    @Override
    public String toString() {
        return imie + " " + nazwisko + ", wiek: " + wiek + ", płeć: " + plec + ", pesel: " + pesel;
    }
    
    // gettery i settery
    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getWiek() {
        return wiek;
    }

    public void setWiek(String wiek) {
        this.wiek = wiek;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }
    
    public String getPlec() {
        return plec;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }
}
