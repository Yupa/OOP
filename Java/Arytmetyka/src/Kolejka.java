/**
 * @author rp386037
 */
public class Kolejka {

    private int[] tab;
    private int iterWyjmij;
    private int iterWstaw;
    private int tabLength;
    
    //startujemy od tablicy wielkosci 2
    //do tablicy o dlugosci k moze znajdowac sie maksymalnie k-1 elementow
    public Kolejka() {
        tab = new int [2];
        tabLength = 2;
        iterWstaw = 0;
    }
    
    //przesuwa iterator na nastepne miejsce w cyklu
    private int powiekszIterator (int iter) {
        iter++;
        if (iter == tabLength)
            return 0;
        else
            return iter;
    }
    
    public boolean czyPusta() {
        return (iterWstaw == iterWyjmij);
    }
    
    //dodatkowa funkcja mowiaca czy zapelnilismy tablice
    private boolean czyPelna() {
        return (powiekszIterator(iterWstaw) == iterWyjmij);
    }
    
    public int getLength() {
        if(iterWstaw >= iterWyjmij)
            return iterWstaw - iterWyjmij;
        else
            return (tabLength + iterWstaw - iterWyjmij);
    }
    
    //podwaja rozmiar tablicy i przenosi konieczne elementy na jej poczatek
    private void podwoj() {
        int[] nowaTablica = new int[tabLength*2];
        
        //kopiowanie elementow
        for (int i = iterWyjmij; i < tabLength - 1; ++i)
            nowaTablica[i-iterWyjmij] = tab[i];
        if (iterWyjmij > iterWstaw) {
            nowaTablica [tabLength - 1 - iterWyjmij] = tab[tabLength - 1];
            for (int i = 0; i < iterWstaw; ++i)
                nowaTablica[i + tabLength - iterWyjmij] = tab[i];
        }
        iterWyjmij = 0;
        iterWstaw = tabLength - 1;
        tab = nowaTablica;
        tabLength = tab.length;
    }
    
    public void wstaw(int nowy) {
        if(czyPelna())
            podwoj();
        tab[iterWstaw] = nowy;
        iterWstaw = powiekszIterator(iterWstaw);
    }
    
    public int pobierz() {
        if (czyPusta())
            return -1;
        int wyn = tab[iterWyjmij];
        iterWyjmij = powiekszIterator (iterWyjmij);
        return wyn;
    }
}
