/**
 * @author rp386037
 */
public class Wielomian {

    private double[] wspolczynnik;
    private int[] wykladnik;
    private int deg;

    public double[] getWspolczynnik() {
        return wspolczynnik;
    }

    public int[] getWykladnik() {
        return wykladnik;
    }

    public int getDeg() {
        return deg;
    }

    //przyjmujemy, ze tablice sa posortowane rosnaco po wykladnikach
    public Wielomian(double[] noweWspolczynniki, int[] noweWykladniki) {
        if (noweWspolczynniki.length != noweWykladniki.length)
            return;
        deg = noweWspolczynniki.length;
        wspolczynnik = new double[deg];
        wykladnik = new int[deg];
        for (int i = 0; i < deg; ++i)
        {
            wspolczynnik[i] = noweWspolczynniki[i];
            wykladnik[i] = noweWykladniki[i];
        }
    }

    public void dodaj(Wielomian doDodania) {
        int iterA = 0, iterB = 0;
        int nowyDeg = deg + doDodania.getDeg();
        //obliczamy stopien nowego wielomianu
        while (iterA < deg && iterB < doDodania.deg) {
            if (wykladnik[iterA] < doDodania.wykladnik[iterB])
                iterA++;
            else if (wykladnik[iterA] > doDodania.wykladnik[iterB])
                iterB++;
            else if (wspolczynnik[iterA] + doDodania.wspolczynnik[iterB] != 0) {
                nowyDeg--;
                iterA++;
                iterB++;
            }
            else {
                nowyDeg -= 2;
                iterA++;
                iterB++;
            }
        }
        double[] nowyWspolczynnik = new double [nowyDeg];
        int[] nowyWykladnik = new int [nowyDeg];
        iterA = iterB = 0;
        int iterC = 0;
        //dodajemy wspolczynniki do nowego wielomianu
        while (iterA < deg && iterB < doDodania.getDeg()) {
            if (wykladnik [iterA] < doDodania.getWykladnik() [iterB]) {
                nowyWykladnik [iterC] = wykladnik [iterA];
                nowyWspolczynnik [iterC] = wspolczynnik [iterA];
                iterC++;
                iterA++;
            }
            else if (wykladnik [iterA] > doDodania.getWykladnik() [iterB]) {
                nowyWykladnik [iterC] = doDodania.getWykladnik() [iterB];
                nowyWspolczynnik [iterC] = doDodania.getWspolczynnik() [iterB];
                iterC++;
                iterB++;
            }
            else if (wspolczynnik [iterA] + doDodania.getWspolczynnik() [iterB] == 0) {
                iterA++;
                iterB++;
            }
            else {
                nowyWykladnik [iterC] = wykladnik [iterA];
                nowyWspolczynnik [iterC] = wspolczynnik [iterA] + 
                    doDodania.getWspolczynnik() [iterB];
                iterA++;
                iterB++;
                iterC++;
            }
        }
        
        //jesli wielomian doDodania skonczyl sie pierwszy to musimy dodac pozostalosci
        while (iterA < deg) {
            nowyWykladnik [iterC] = wykladnik [iterA];
            nowyWspolczynnik [iterC] = wspolczynnik [iterA];
            iterA++;
            iterC++;
        }
        //jesli to nasz wielomian pierwszy sie skonczyl
        while (iterB < doDodania.getDeg()) {
            nowyWykladnik [iterC] = doDodania.getWykladnik() [iterB];
            nowyWspolczynnik [iterC] = doDodania.getWspolczynnik() [iterB];
            iterB++;
            iterC++;
        }
        wspolczynnik = nowyWspolczynnik;
        wykladnik = nowyWykladnik;
        deg = nowyDeg;
    }

    public double wylicz(double x) {
        double wyn = 0;
        for (int i = 0; i < wspolczynnik.length; ++i) {
            wyn += wspolczynnik[i]*Math.pow(x, wykladnik[i]);
        }
        return wyn;
    }
    
    public void wypisz() {
        if (deg > 0) {
            System.out.print(wspolczynnik[0]);
            for(int i = 1; i < deg; ++i) {
                System.out.print((wspolczynnik[i] > 0 ? " +" : " ") + wspolczynnik[i] + "x^" + wykladnik[i]);
            }
            System.out.println();
        }
        else {
            System.out.println(0);
        }
    }
}
