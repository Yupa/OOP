import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rp386037
 */
public class KolejkaTest {

    /**
     * Test of czyPusta method, of class Kolejka.
     */
    @Test
    public void testCzyPusta() {
        System.out.println("czyPusta");
        Kolejka instance = new Kolejka();
        instance.wstaw(18);
        instance.pobierz();
        instance.wstaw(1);
        boolean expResult = false;
        boolean result = instance.czyPusta();
        assertEquals(expResult, result);
        
        //dla pustej kolejki
        instance = new Kolejka();
        expResult = true;
        result = instance.czyPusta();
        assertEquals(expResult, result);
        
        //dla pustej kolejki
        instance.wstaw(1);
        instance.pobierz();
        expResult = true;
        result = instance.czyPusta();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLength method, of class Kolejka.
     */
    @Test
    public void testGetLength() {
        System.out.println("getLength");
        //pusta kolejka
        Kolejka instance = new Kolejka();
        instance.wstaw(24);
        instance.pobierz();
        int expResult = 0;
        int result = instance.getLength();
        assertEquals(expResult, result);
        
        instance = new Kolejka();
        instance.wstaw(17);
        instance.wstaw(24);
        instance.pobierz();
        expResult = 1;
        result = instance.getLength();
        assertEquals(expResult, result);
        
        instance = new Kolejka();
        instance.wstaw(17);
        instance.wstaw(24);
        instance.wstaw(21);
        instance.wstaw(21);
        expResult = 4;
        result = instance.getLength();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of pobierz method, of class Kolejka.
     */
    @Test
    public void testWstawIPobierz() {
        System.out.println("wstaw i pobierz");
        //dla pustej kolejki
        Kolejka instance = new Kolejka();
        int expResult = -1;
        int result = instance.pobierz();
        assertEquals(expResult, result);

        instance = new Kolejka();
        instance.wstaw(17);
        instance.wstaw(27);
        expResult = 17;
        result = instance.pobierz();
        assertEquals(expResult, result);
        
        instance = new Kolejka();
        instance.wstaw(17);
        instance.pobierz();
        instance.wstaw(27);
        expResult = 27;
        result = instance.pobierz();
        assertEquals(expResult, result);
    }
}
