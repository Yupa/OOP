import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rp386037
 */
public class WielomianTest {
    //wielomiany w1 i w2 to losowe wielomiany, w3 = w1 + w2, w11 = w1 + (-w1)
    Wielomian w1, w2, w3, w11;
    double[] wsp1, wsp2, wsp3, wsp11;
    int[] wyk1, wyk2, wyk3;
    double EPSILON = 0.000001;
    
    public WielomianTest() {
        wsp1 = new double[3];  wyk1 = new int[3];
        wsp1[0] = 1.2;  wsp1[1] = 2.3;   wsp1[2] = 4.5;
        wyk1[0] = 0;    wyk1[1] = 3;     wyk1[2] = 7;
        
        wsp2 = new double[4];  wyk2 = new int[4];
        wsp2[0] = 1.6;  wsp2[1] = -2.3;  wsp2[2] = 4.3;  wsp2[3] = 1;
        wyk2[0] = 2;    wyk2[1] = 3;     wyk2[2] = 6;    wyk2[3] = 7;
        
        wsp3 = new double[4];  wyk3 = new int[4];
        wsp3[0] = 1.2;  wsp3[1] = 1.6;   wsp3[2] = 4.3;  wsp3[3] = 5.5;
        wyk3[0] = 0;    wyk3[1] = 2;     wyk3[2] = 6;    wyk3[3] = 7;
        
        w1 = new Wielomian(wsp1, wyk1);
        w2 = new Wielomian(wsp2, wyk2);
        w3 = new Wielomian(wsp1, wyk1);
        w3.dodaj(w2);
        
        wsp11 = new double[3];
        wsp11[0] = -1.2;  wsp11[1] = -2.3;   wsp11[2] = -4.5;
        w11 = new Wielomian(wsp11, wyk1);
        w11.dodaj(w1);
    }

    /**
     * Test of getWspolczynnik method, of class Wielomian.
     */
    @Test
    public void testGetWspolczynnik() {
        System.out.println("getWspolczynnik");
        //wspolczynniki wielomianow w1 i w2
        double[] expResult = wsp1;
        double[] result = w1.getWspolczynnik();
        assertArrayEquals(expResult, result, EPSILON);
        
        expResult = wsp2;
        result = w2.getWspolczynnik();
        assertArrayEquals(expResult, result, EPSILON);
    }

    /**
     * Test of getWykladnik method, of class Wielomian.
     */
    @Test
    public void testGetWykladnik() {
        System.out.println("getWykladnik");
        //wykladniki wielomianow w1 i w2
        int[] expResult = wyk1;
        int[] result = w1.getWykladnik();
        assertArrayEquals(expResult, result);
        
        expResult = wyk2;
        result = w2.getWykladnik();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of getDeg method, of class Wielomian.
     */
    @Test
    public void testGetDeg() {
        System.out.println("getDeg");
        //stopnie wielomianow w1 i w2
        int expResult = 3;
        int result = w1.getDeg();
        assertEquals(expResult, result);
        
        expResult = 4;
        result = w2.getDeg();
        assertEquals(expResult, result);
    }

    /**
     * Test of dodaj method, of class Wielomian.
     */
    @Test
    public void testDodaj() {
        System.out.println("dodaj");
        //sprawdzanie stopnia w1 + w2
        {
            int expResult = 4;
            int result = w3.getDeg();
            assertEquals(expResult, result);
        }
        //sprawdzanie wykladnikow w1 + w2
        {
            int[] expResult = wyk3;
            int[] result = w3.getWykladnik();
            assertArrayEquals(expResult, result);
        }
        //sprawdzanie wspolczynnikow w1 + w2
        {
            double[] expResult = wsp3;
            double[] result = w3.getWspolczynnik();
            assertArrayEquals(expResult, result, EPSILON);
        }
        //sprawdzanie wartosci w1 + w2 dla x = 0, 1, 2
        {
            double x = 0.0;
            double expResult = 1.2;
            double result = w3.wylicz(x);
            assertEquals(expResult, result, EPSILON);
            
            x = 1.0;
            expResult = 12.6;
            result = w3.wylicz(x);
            assertEquals(expResult, result, EPSILON);
            
            x = 2.0;
            expResult = 986.8;
            result = w3.wylicz(x);
            assertEquals(expResult, result, EPSILON);
        }
        
        //sprawdzanie stopnia w1 + (-w1)
        {
            int expResult = 0;
            int result = w11.getDeg();
            assertEquals(expResult, result);
        }
        //sprawdzanie wykladnikow w1 + (-w1)
        {
            int[] expResult = new int[0];
            int[] result = w11.getWykladnik();
            assertArrayEquals(expResult, result);
        }
        //sprawdzanie wspolczynnikow w1 + (-w1)
        {
            double[] expResult = new double[0];
            double[] result = w11.getWspolczynnik();
            assertArrayEquals(expResult, result, EPSILON);
        }
        //sprawdzanie wartosci w1 + (-w1) dla x = 0, 1, 2
        {
            double x = 0.0;
            double expResult = 0.0;
            double result = w11.wylicz(x);
            assertEquals(expResult, result, EPSILON);
            
            x = 1.0;
            expResult = 0.0;
            result = w11.wylicz(x);
            assertEquals(expResult, result, EPSILON);
            
            x = 2.0;
            expResult = 0.0;
            result = w11.wylicz(x);
            assertEquals(expResult, result, EPSILON);
        }
    }

    /**
     * Test of wylicz method, of class Wielomian.
     */
    @Test
    public void testWylicz() {
        System.out.println("wylicz");
        double x, expResult, result;
        
        //sprawdzanie wartosci w1 dla 0, 1, 2
        x = 0.0;
        expResult = 1.2;
        result = w1.wylicz(x);
        assertEquals(expResult, result, EPSILON);
        
        x = 1.0;
        expResult = 8.0;
        result = w1.wylicz(x);
        assertEquals(expResult, result, EPSILON);
        
        x = 2.0;
        expResult = 595.6;
        result = w1.wylicz(x);
        assertEquals(expResult, result, EPSILON);
        
        //sprawdzanie wartosci w2 dla 0, 1, 2
        x = 0.0;
        expResult = 0.0;
        result = w2.wylicz(x);
        assertEquals(expResult, result, EPSILON);
        
        x = 1.0;
        expResult = 4.6;
        result = w2.wylicz(x);
        assertEquals(expResult, result, EPSILON);
        
        x = 2.0;
        expResult = 391.2;
        result = w2.wylicz(x);
        assertEquals(expResult, result, EPSILON);
    }
    
    /**
     * Test of wypisz method, of class Wielomian.
     */
    @Test
    public void testWypisz() {
        System.out.println("wypisz");
        //1.2 +2.3x^3 +4.5x^7
        w1.wypisz();
        //1.6 -2.3x^3 +4.3x^6 +1.0x^7
        w2.wypisz();
        //wypisanie wielomianu pustego
        w11.wypisz();
    }
}
