package figura;

public class Rectangle extends Figure {
    private final double[] verX;
    private final double[] verY;

    public Rectangle(double[] v) {
        this.verX = new double[] {v[0], v[2]};
        this.verY = new double[] {v[1], v[3]};
    }
    
    @Override
    public String toString() {
        return "Rectangle - Vertices: (" + 
            verX[0] + ", " + verY[0] + ") , (" + verX[0] + ", " + verY[1] + ") , (" + 
            verX[1] + ", " + verY[1] + ")" + ") , (" + verX[1] + ", " + verY[0] + ")";
    }
    
    @Override
    public double calculateArea() {
        double a = super.calculateLength(verX[0], verY[0], verX[0], verY[1]);
        double b = super.calculateLength(verX[1], verY[0], verX[1], verY[1]);
        return a*b;
    }
}
