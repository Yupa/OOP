package figura;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public abstract class Figure {

    public static final String FILE_NAME = "input.txt";
    
    public static void main(String[] args) {
        //try with resources zapewnia nam zwolnienie zasobow pliku
        try (Scanner scan = new Scanner(new File(FILE_NAME))){
            //wczytuje ilosc figur
            int n = scan.nextInt();
            scan.nextLine();
            //figury sa trzymane w tablicy
            Figure[] figures = new Figure [n];
            for (int i = 0; i < n; ++i) {
                String s = scan.nextLine();
                try (Scanner scan2 = new Scanner(s)) {
                    //wczytuje znak
                    String sign;
                    sign = scan2.next(); 
                    Figure newFigure;
                    if (sign.equals("K")) {
                        double a, b, c;
                        a = scan2.nextDouble();
                        b = scan2.nextDouble();
                        c = scan2.nextDouble();
                        newFigure = new Circle (a, b, c);
                    }
                    else if (sign.equals("T")) {
                        double[] vert = new double [6];
                        for (int j = 0; j < 6; j++)
                            vert[j] = scan2.nextDouble();
                        newFigure = new Triangle (vert);
                    }
                    else if (sign.equals("P")) {
                        //prostokat inicjalizujemy lewym dolnym oraz prawym gornym punktem
                        double[] vert = new double [4];
                        for (int j = 0; j < 4; j++)
                            vert[j] = scan2.nextDouble();
                        newFigure = new Rectangle (vert);
                    }
                    else throw new InputMismatchException();
                    figures [i] = newFigure;
                }
            }
            //oblicza sume pol
            double sum = 0;
            for (int i = 0; i < n; i++) {
                sum += figures[i].calculateArea();
            }
            System.out.println("Suma pól = " + sum);
            //wypisuje opisy figur
            for (int i = 0; i < n; i++) {
                System.out.println(figures[i].toString());
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Nie ma takiego pliku");
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            System.out.println("Scanner został zamknięty");
            e.printStackTrace();
        }
        catch (InputMismatchException e) {
            System.out.println("Błędne dane wejściowe");
            e.printStackTrace();
        }
        catch (NoSuchElementException e) {
            System.out.println("Koniec pliku");
            e.printStackTrace();
        }
    }
    
    //pomocnicza funkcja liczaca odleglosc miedzy dwoma punktami
    protected double calculateLength(double a1, double a2, double b1, double b2) {
        return Math.sqrt((a1 - b1)*(a1 - b1) + (a2 - b2)*(a2 - b2));
    }
    
    public abstract double calculateArea();
    
    @Override
    public abstract String toString();
}
