package figura;

public class Circle extends Figure{
    private final double cenX;
    private final double cenY;
    private final double rad;

    public Circle(double a, double b, double c) {
        this.cenX = a;
        this.cenY = b;
        this.rad = c;
    }
    
    @Override
    public String toString() {
        return "Circle - Centre: (" + cenX + ", " + cenY + ") , Radius: " + rad;
    }
    
    @Override
    public double calculateArea() {
        return Math.PI*rad*rad;
    }
}
