package figura;

public class Triangle extends Figure{
    private final double[] verX;
    private final double[] verY;

    public Triangle(double[] v) {
        this.verX = new double[] {v[0], v[2], v[4]};
        this.verY = new double[] {v[1], v[3], v[5]};
    }
    
    @Override
    public String toString() {
        return "Triangle - Vertices: (" + verX[0] + ", " + verY[0] + ") , (" +
            verX[1] + ", " + verY[1] + ") , (" + verX[2] + ", " + verY[2] + ")";
    }
    
    @Override
    public double calculateArea() {
        double a = super.calculateLength(verX[0], verY[0], verX[1], verY[1]);
        double b = super.calculateLength(verX[1], verY[1], verX[2], verY[2]);
        double c = super.calculateLength(verX[2], verY[2], verX[0], verY[0]);
        double p = (a + b + c)/2;
        return Math.sqrt(p*(p-a)*(p-b)*(p-c));
    }
}
