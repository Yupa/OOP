package main.java;

import main.scala.*;

public interface Maszyna {
    public void mieszaj(Farba farba);
    public boolean uzyjPigmentu(Farba farba, Pigment pigment);
    public void zakonczMieszanie();
}
