package main.java;

import main.scala.*;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class GUI extends javax.swing.JFrame {
    private Maszyna maszyna;
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            	// Wczytujemy konfigurację poczatkowa (maszyna.conf), jeśli się nie uda to kończymy z błędem
                KonfiguracjaMaszyny konfiguracja = new KonfiguracjaMaszyny();
                if(!konfiguracja.wczytaj())
                    System.exit(-1);
                // Tworzymy nowe gui i przekazujemy odpowiedni obiekt, aby dokonczyc konfiguracje
                GUI gui = new GUI(konfiguracja);
                gui.setVisible(true);
            }
        });
    }
    
    public GUI(KonfiguracjaMaszyny k) {
        this.maszyna = k.getMaszyna();
        // Tworzymy komponenty okna
        initComponents();
        // Tworzymy listenery, które współdziałają z beanem
        initListeners();
        // Jeśli nie uda sie skonfigurowac farb i pigmentow to konczymy dzialanie
        if(!k.wczytajFarbyIPigmenty(magazynBean)) {
            System.out.println("KONFIGURACJA NIEUDANA");
        	System.exit(-1);
        }
    }

    // Tworzenie komponentow - kod autogenerowany przez netbeansa
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        magazynBean = new Magazyn();
        farbaPanel = new javax.swing.JPanel();
        kolorFarbaLabel = new javax.swing.JLabel();
        kolorFarbaPanel = new javax.swing.JScrollPane();
        kolorFarbaText = new javax.swing.JTextArea();
        toksycznoscFarbaLabel = new javax.swing.JLabel();
        jakoscFarbaLabel = new javax.swing.JLabel();
        jakoscFarbaSpinner = new javax.swing.JSpinner();
        toksycznoscFarbaSpinner = new javax.swing.JSpinner();
        pigmentPanel = new javax.swing.JPanel();
        toksycznoscPigmentLabel = new javax.swing.JLabel();
        toksycznoscPigmentComboBox = new javax.swing.JComboBox<>();
        toksycznoscPigmentSpinner = new javax.swing.JSpinner();
        jakoscPigmentLabel = new javax.swing.JLabel();
        jakoscPigmentComboBox = new javax.swing.JComboBox<>();
        jakoscPigmentSpinner = new javax.swing.JSpinner();
        nazwaPIgmentLabel = new javax.swing.JLabel();
        nazwaPigmentPanel = new javax.swing.JScrollPane();
        nazwaPigmentText = new javax.swing.JTextArea();
        regulyLabel = new javax.swing.JLabel();
        regulaWyjscieText = new javax.swing.JTextField();
        dodajReguleButton = new javax.swing.JButton();
        regulaWejscieText = new javax.swing.JTextField();
        strzalkaLabel = new javax.swing.JLabel();
        farbyPanel = new javax.swing.JPanel();
        farbyListPanel = new javax.swing.JScrollPane();
        farbyList = new javax.swing.JList<>();
        pigmentyPanel = new javax.swing.JPanel();
        pigmentyListPanel = new javax.swing.JScrollPane();
        pigmentyList = new javax.swing.JList<>();
        przyciskiPanel = new javax.swing.JPanel();
        dodajPigmentButton = new javax.swing.JButton();
        dodajFarbeButton = new javax.swing.JButton();
        mieszajButton = new javax.swing.JButton();
        uzyjPigmentuButton = new javax.swing.JButton();
        edytujFarbeToggleButton = new javax.swing.JToggleButton();
        edytujPigmentToggleButton = new javax.swing.JToggleButton();
        pigmentyPanel1 = new javax.swing.JPanel();
        pigmentyListPanel1 = new javax.swing.JScrollPane();
        regulyList = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        farbaPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Farba"));

        kolorFarbaLabel.setText("Kolor");

        kolorFarbaText.setEditable(false);
        kolorFarbaText.setColumns(1);
        kolorFarbaText.setRows(1);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaFarba.kolor}"), kolorFarbaText, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        kolorFarbaPanel.setViewportView(kolorFarbaText);

        toksycznoscFarbaLabel.setText("Toksyczność");

        jakoscFarbaLabel.setText("Jakość");

        jakoscFarbaSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, 100.0d, 1.0d));
        jakoscFarbaSpinner.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jakoscFarbaSpinner.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaFarba.jakosc}"), jakoscFarbaSpinner, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        toksycznoscFarbaSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, 0.0d, 100.0d, 1.0d));
        toksycznoscFarbaSpinner.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        toksycznoscFarbaSpinner.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaFarba.toksycznosc}"), toksycznoscFarbaSpinner, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        javax.swing.GroupLayout farbaPanelLayout = new javax.swing.GroupLayout(farbaPanel);
        farbaPanel.setLayout(farbaPanelLayout);
        farbaPanelLayout.setHorizontalGroup(
            farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(farbaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(kolorFarbaLabel)
                    .addComponent(toksycznoscFarbaLabel)
                    .addComponent(jakoscFarbaLabel))
                .addGroup(farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(farbaPanelLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jakoscFarbaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(farbaPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(kolorFarbaPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(farbaPanelLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(toksycznoscFarbaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(132, Short.MAX_VALUE))
        );
        farbaPanelLayout.setVerticalGroup(
            farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(farbaPanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(kolorFarbaLabel)
                    .addComponent(kolorFarbaPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(toksycznoscFarbaLabel)
                    .addComponent(toksycznoscFarbaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(farbaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jakoscFarbaLabel)
                    .addComponent(jakoscFarbaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pigmentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Pigment"));

        toksycznoscPigmentLabel.setText("Zmiana toksyczności");

        toksycznoscPigmentComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "+", "-", "x" }));
        toksycznoscPigmentComboBox.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.znakToksycznosc}"), toksycznoscPigmentComboBox, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);

        toksycznoscPigmentSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 1.0d));
        toksycznoscPigmentSpinner.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.toksycznosc}"), toksycznoscPigmentSpinner, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        jakoscPigmentLabel.setText("Zmiana jakości");

        jakoscPigmentComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "+", "-", "x" }));
        jakoscPigmentComboBox.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.znakJakosc}"), jakoscPigmentComboBox, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);

        jakoscPigmentSpinner.setModel(new javax.swing.SpinnerNumberModel(0.0d, null, null, 1.0d));
        jakoscPigmentSpinner.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.jakosc}"), jakoscPigmentSpinner, org.jdesktop.beansbinding.BeanProperty.create("value"));
        bindingGroup.addBinding(binding);

        nazwaPIgmentLabel.setText("Nazwa");

        nazwaPigmentText.setEditable(false);
        nazwaPigmentText.setColumns(1);
        nazwaPigmentText.setRows(1);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnyPigment.nazwa}"), nazwaPigmentText, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        nazwaPigmentPanel.setViewportView(nazwaPigmentText);

        regulyLabel.setText("Reguły");

        regulaWyjscieText.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.farbaWyjsciowa}"), regulaWyjscieText, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        dodajReguleButton.setText("Dodaj");
        dodajReguleButton.setEnabled(false);
        dodajReguleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajReguleButtonActionPerformed(evt);
            }
        });

        regulaWejscieText.setEnabled(false);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, org.jdesktop.beansbinding.ELProperty.create("${obecnaRegula.farbaWejsciowa}"), regulaWejscieText, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        strzalkaLabel.setText("->");

        javax.swing.GroupLayout pigmentPanelLayout = new javax.swing.GroupLayout(pigmentPanel);
        pigmentPanel.setLayout(pigmentPanelLayout);
        pigmentPanelLayout.setHorizontalGroup(
            pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pigmentPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(toksycznoscPigmentLabel)
                    .addComponent(nazwaPIgmentLabel)
                    .addComponent(jakoscPigmentLabel)
                    .addGroup(pigmentPanelLayout.createSequentialGroup()
                        .addComponent(regulyLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                        .addComponent(dodajReguleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(35, 35, 35)
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pigmentPanelLayout.createSequentialGroup()
                        .addComponent(nazwaPigmentPanel)
                        .addGap(36, 36, 36))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pigmentPanelLayout.createSequentialGroup()
                        .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jakoscPigmentComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(toksycznoscPigmentComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(regulaWejscieText, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(toksycznoscPigmentSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jakoscPigmentSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pigmentPanelLayout.createSequentialGroup()
                                .addComponent(strzalkaLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(regulaWyjscieText, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(35, 35, 35))))
        );
        pigmentPanelLayout.setVerticalGroup(
            pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pigmentPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nazwaPIgmentLabel)
                    .addComponent(nazwaPigmentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(toksycznoscPigmentLabel)
                        .addComponent(toksycznoscPigmentSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(toksycznoscPigmentComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jakoscPigmentLabel)
                    .addComponent(jakoscPigmentComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jakoscPigmentSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pigmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(regulyLabel)
                    .addComponent(regulaWyjscieText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dodajReguleButton)
                    .addComponent(regulaWejscieText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(strzalkaLabel))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        farbyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Farby"));

        farbyList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        farbyList.setToolTipText("");

        org.jdesktop.beansbinding.ELProperty eLProperty = org.jdesktop.beansbinding.ELProperty.create("${listaFarb}");
        org.jdesktop.swingbinding.JListBinding jListBinding = org.jdesktop.swingbinding.SwingBindings.createJListBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, magazynBean, eLProperty, farbyList);
        jListBinding.setDetailBinding(org.jdesktop.beansbinding.ELProperty.create("${kolor}"));
        bindingGroup.addBinding(jListBinding);

        farbyList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                farbyListValueChanged(evt);
            }
        });
        farbyListPanel.setViewportView(farbyList);

        javax.swing.GroupLayout farbyPanelLayout = new javax.swing.GroupLayout(farbyPanel);
        farbyPanel.setLayout(farbyPanelLayout);
        farbyPanelLayout.setHorizontalGroup(
            farbyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(farbyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(farbyListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                .addContainerGap())
        );
        farbyPanelLayout.setVerticalGroup(
            farbyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(farbyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(farbyListPanel)
                .addContainerGap())
        );

        pigmentyPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Pigmenty"));

        pigmentyList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        pigmentyList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                pigmentyListValueChanged(evt);
            }
        });
        pigmentyListPanel.setViewportView(pigmentyList);

        javax.swing.GroupLayout pigmentyPanelLayout = new javax.swing.GroupLayout(pigmentyPanel);
        pigmentyPanel.setLayout(pigmentyPanelLayout);
        pigmentyPanelLayout.setHorizontalGroup(
            pigmentyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pigmentyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pigmentyListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                .addContainerGap())
        );
        pigmentyPanelLayout.setVerticalGroup(
            pigmentyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pigmentyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pigmentyListPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
        );

        dodajPigmentButton.setText("Dodaj Pigment");
        dodajPigmentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajPigmentButtonActionPerformed(evt);
            }
        });

        dodajFarbeButton.setText("Dodaj Farbę");
        dodajFarbeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dodajFarbeButtonActionPerformed(evt);
            }
        });

        mieszajButton.setText("Mieszaj");
        mieszajButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mieszajButtonActionPerformed(evt);
            }
        });

        uzyjPigmentuButton.setText("Użyj Pigmentu");
        uzyjPigmentuButton.setEnabled(false);
        uzyjPigmentuButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uzyjPigmentuButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout przyciskiPanelLayout = new javax.swing.GroupLayout(przyciskiPanel);
        przyciskiPanel.setLayout(przyciskiPanelLayout);
        przyciskiPanelLayout.setHorizontalGroup(
            przyciskiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(przyciskiPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(przyciskiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(dodajPigmentButton, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                    .addComponent(dodajFarbeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mieszajButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(uzyjPigmentuButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        przyciskiPanelLayout.setVerticalGroup(
            przyciskiPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(przyciskiPanelLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(dodajFarbeButton)
                .addGap(18, 18, 18)
                .addComponent(dodajPigmentButton)
                .addGap(18, 18, 18)
                .addComponent(mieszajButton)
                .addGap(18, 18, 18)
                .addComponent(uzyjPigmentuButton)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        edytujFarbeToggleButton.setText("Edytuj Farbę");
        edytujFarbeToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edytujFarbeToggleButtonActionPerformed(evt);
            }
        });

        edytujPigmentToggleButton.setText("Edytuj Pigment");
        edytujPigmentToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                edytujPigmentToggleButtonActionPerformed(evt);
            }
        });

        pigmentyPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Reguly"));

        regulyList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        regulyList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                regulyListValueChanged(evt);
            }
        });
        pigmentyListPanel1.setViewportView(regulyList);

        javax.swing.GroupLayout pigmentyPanel1Layout = new javax.swing.GroupLayout(pigmentyPanel1);
        pigmentyPanel1.setLayout(pigmentyPanel1Layout);
        pigmentyPanel1Layout.setHorizontalGroup(
            pigmentyPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pigmentyPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pigmentyListPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addContainerGap())
        );
        pigmentyPanel1Layout.setVerticalGroup(
            pigmentyPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pigmentyPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pigmentyListPanel1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(farbaPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(edytujFarbeToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(farbyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45)
                        .addComponent(pigmentyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(edytujPigmentToggleButton, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pigmentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(pigmentyPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(przyciskiPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pigmentyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(farbyPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pigmentyPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(przyciskiPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(edytujFarbeToggleButton)
                    .addComponent(edytujPigmentToggleButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(farbaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pigmentPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Tworzenie listenerow do nasluchiwania beana
    private void initListeners() {
        // Zmiana listy farb
    	PropertyChangeListener farbyChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DefaultListModel model1 = new DefaultListModel();
                for (Farba f : magazynBean.getListaFarb()) {
                    model1.addElement(f);
                }
                farbyList.setModel(model1);
            }
        };
        magazynBean.addListaFarbChangeListener(farbyChangeListener);
        
        // Zmiana listy pigmentów
        PropertyChangeListener pigmentyChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DefaultListModel model1 = new DefaultListModel();
                for (Pigment p : magazynBean.getListaPigmentow()) {
                    model1.addElement(p);
                }   
                pigmentyList.setModel(model1);
            }
        };
        magazynBean.addListaPigmentowChangeListener(pigmentyChangeListener);
        
        // Zmiana obecnej farby
        PropertyChangeListener obecnaFarbaChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                kolorFarbaText.setText(magazynBean.getObecnaFarba().getKolor());
                toksycznoscFarbaSpinner.setValue(magazynBean.getObecnaFarba().getToksycznosc());
                jakoscFarbaSpinner.setValue(magazynBean.getObecnaFarba().getJakosc());
            }
        };
        magazynBean.addObecnaFarbaChangeListener(obecnaFarbaChangeListener);
        
        // Zmiana obecnego pigmentu
        PropertyChangeListener obecnyPigmentChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                nazwaPigmentText.setText(magazynBean.getObecnyPigment().getNazwa());
            }
        };
        magazynBean.addObecnyPigmentChangeListener(obecnyPigmentChangeListener);
        
        // Jesli wybieramy inna farbe z listy to dezaktywujemy przycisk uzyj pigmentu
        MouseListener mouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent mouseEvent) {
                zabronPigmentu("kliknięto na listę");
            }
        };
        farbyList.addMouseListener(mouseListener);
        
        // Zmiana listy regul
        PropertyChangeListener regulyChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                DefaultListModel model1 = new DefaultListModel();
                for (Regula r : magazynBean.getListaRegul()) {
                    model1.addElement(r);
                }   
                regulyList.setModel(model1);
            }
        };
        magazynBean.addListaRegulChangeListener(regulyChangeListener);
        
        // Zmiana obecnej reguly
        PropertyChangeListener obecnyRegulaChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                regulaWejscieText.setText(magazynBean.getObecnaRegula().getFarbaWejsciowa());
                regulaWyjscieText.setText(magazynBean.getObecnaRegula().getFarbaWyjsciowa());
                toksycznoscPigmentComboBox.setSelectedItem(magazynBean.getObecnaRegula().getZnakToksycznosc());
                toksycznoscPigmentSpinner.setValue(magazynBean.getObecnaRegula().getToksycznosc());
                jakoscPigmentComboBox.setSelectedItem(magazynBean.getObecnaRegula().getZnakJakosc());
                jakoscPigmentSpinner.setValue(magazynBean.getObecnaRegula().getJakosc());
            }
        };
        magazynBean.addObecnaRegulaChangeListener(obecnyRegulaChangeListener);
    }
    
    // Dezaktywowanie przycisku uzyj pigmentu
    private void zabronPigmentu(String ktoZabrania) {
    	// ktoZabrania mowi nam dlaczego nie mozna dalej uzywac pigmentu
    	// System.out.println("Nie można dalej mieszać, bo " + ktoZabrania);
        if(uzyjPigmentuButton.isEnabled()) {
            maszyna.zakonczMieszanie();
            uzyjPigmentuButton.setEnabled(false);
        }
    }
    
    // Aktywowanie przycisku uzyj pigmentu
    private void pozwolNaPigment() {
        uzyjPigmentuButton.setEnabled(true);
    }
    
    // Dodaje losowa farbe
    private void dodajFarbeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajFarbeButtonActionPerformed
    	magazynBean.stworzLosowaFarbe();
        zabronPigmentu("dodaje farbe");
    }//GEN-LAST:event_dodajFarbeButtonActionPerformed

    // Dodaje losowy pigment
    private void dodajPigmentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajPigmentButtonActionPerformed
        magazynBean.stworzLosowyPigment();
        zabronPigmentu("dodaje pigment");
    }//GEN-LAST:event_dodajPigmentButtonActionPerformed

    // Rozpoczyna mieszanie
    private void mieszajButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mieszajButtonActionPerformed
        maszyna.mieszaj(magazynBean.getObecnaFarba());
        pozwolNaPigment();
        // Jeśli byliśmy w trakcie edycji farby/pigmentu to ją dezaktywujemy
        if(edytujFarbeToggleButton.isSelected())
            edytujFarbeToggleButton.doClick();
        if(edytujPigmentToggleButton.isSelected())
            edytujPigmentToggleButton.doClick();
    }//GEN-LAST:event_mieszajButtonActionPerformed

    // Dodaje pigment
    private void uzyjPigmentuButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uzyjPigmentuButtonActionPerformed
        // Sprawdzamy czy mozemy uzyc pigmentu z dana farba
    	if(!maszyna.uzyjPigmentu(magazynBean.getObecnaFarba(), magazynBean.getObecnyPigment())) {
            JOptionPane.showMessageDialog(this, "Nie można zmieszać tego pigmentu z tą farbą.", "Błędny Pigment", JOptionPane.WARNING_MESSAGE);
        }
        else {
            magazynBean.setObecnaFarba(magazynBean.getObecnaFarba());
            magazynBean.setListaFarb(magazynBean.getListaFarb());
        }
    }//GEN-LAST:event_uzyjPigmentuButtonActionPerformed

    // Wybieranie farby z listy
    private void farbyListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_farbyListValueChanged
        if(farbyList.getSelectedIndex() == -1)
            return;
        magazynBean.setObecnaFarba(magazynBean.getListaFarb().get(farbyList.getSelectedIndex()));
    }//GEN-LAST:event_farbyListValueChanged

    // Wybieranie pigmentu z listy
    private void pigmentyListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_pigmentyListValueChanged
        if(pigmentyList.getSelectedIndex() == -1)
            return;
        magazynBean.setObecnyPigment(magazynBean.getListaPigmentow().get(pigmentyList.getSelectedIndex()));
    }//GEN-LAST:event_pigmentyListValueChanged

    // Wlacza / wylacza edycje obecnej farby
    private void edytujFarbeToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edytujFarbeToggleButtonActionPerformed
        if(edytujFarbeToggleButton.isSelected()) {
            zabronPigmentu("Edycja Farby");
            toksycznoscFarbaSpinner.setEnabled(true);
            jakoscFarbaSpinner.setEnabled(true);
        }
        else {
            toksycznoscFarbaSpinner.setEnabled(false);
            jakoscFarbaSpinner.setEnabled(false);
        }
    }//GEN-LAST:event_edytujFarbeToggleButtonActionPerformed

    // Wlacza / wylacza edycje obecnego pigmentu
    private void edytujPigmentToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_edytujPigmentToggleButtonActionPerformed
        if(edytujPigmentToggleButton.isSelected()) {
            zabronPigmentu("Edycja Pigmentu");
            toksycznoscPigmentComboBox.setEnabled(true);
            jakoscPigmentComboBox.setEnabled(true);
            toksycznoscPigmentSpinner.setEnabled(true);
            jakoscPigmentSpinner.setEnabled(true);
            dodajReguleButton.setEnabled(true);
            regulaWejscieText.setEnabled(true);
            regulaWyjscieText.setEnabled(true);
        }
        else {
            toksycznoscPigmentComboBox.setEnabled(false);
            jakoscPigmentComboBox.setEnabled(false);
            toksycznoscPigmentSpinner.setEnabled(false);
            jakoscPigmentSpinner.setEnabled(false);
            dodajReguleButton.setEnabled(false);
            regulaWejscieText.setEnabled(false);
            regulaWyjscieText.setEnabled(false);
        }
    }//GEN-LAST:event_edytujPigmentToggleButtonActionPerformed

    // Dodaje losowa regule do obecnego pigmentu
    private void dodajReguleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dodajReguleButtonActionPerformed
        magazynBean.stworzLosowaRegule(magazynBean.getObecnyPigment());
        zabronPigmentu("dodaje regule");
    }//GEN-LAST:event_dodajReguleButtonActionPerformed

    // Wybieranie reguly z listy
    private void regulyListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_regulyListValueChanged
        if(regulyList.getSelectedIndex() == -1)
            return;
        magazynBean.setObecnaRegula(magazynBean.getListaRegul().get(regulyList.getSelectedIndex()));
    }//GEN-LAST:event_regulyListValueChanged
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton dodajFarbeButton;
    private javax.swing.JButton dodajPigmentButton;
    private javax.swing.JButton dodajReguleButton;
    private javax.swing.JToggleButton edytujFarbeToggleButton;
    private javax.swing.JToggleButton edytujPigmentToggleButton;
    private javax.swing.JPanel farbaPanel;
    private javax.swing.JList<String> farbyList;
    private javax.swing.JScrollPane farbyListPanel;
    private javax.swing.JPanel farbyPanel;
    private javax.swing.JLabel jakoscFarbaLabel;
    private javax.swing.JSpinner jakoscFarbaSpinner;
    private javax.swing.JComboBox<String> jakoscPigmentComboBox;
    private javax.swing.JLabel jakoscPigmentLabel;
    private javax.swing.JSpinner jakoscPigmentSpinner;
    private javax.swing.JLabel kolorFarbaLabel;
    private javax.swing.JScrollPane kolorFarbaPanel;
    private javax.swing.JTextArea kolorFarbaText;
    private Magazyn magazynBean;
    private javax.swing.JButton mieszajButton;
    private javax.swing.JLabel nazwaPIgmentLabel;
    private javax.swing.JScrollPane nazwaPigmentPanel;
    private javax.swing.JTextArea nazwaPigmentText;
    private javax.swing.JPanel pigmentPanel;
    private javax.swing.JList<String> pigmentyList;
    private javax.swing.JScrollPane pigmentyListPanel;
    private javax.swing.JScrollPane pigmentyListPanel1;
    private javax.swing.JPanel pigmentyPanel;
    private javax.swing.JPanel pigmentyPanel1;
    private javax.swing.JPanel przyciskiPanel;
    private javax.swing.JTextField regulaWejscieText;
    private javax.swing.JTextField regulaWyjscieText;
    private javax.swing.JLabel regulyLabel;
    private javax.swing.JList<String> regulyList;
    private javax.swing.JLabel strzalkaLabel;
    private javax.swing.JLabel toksycznoscFarbaLabel;
    private javax.swing.JSpinner toksycznoscFarbaSpinner;
    private javax.swing.JComboBox<String> toksycznoscPigmentComboBox;
    private javax.swing.JLabel toksycznoscPigmentLabel;
    private javax.swing.JSpinner toksycznoscPigmentSpinner;
    private javax.swing.JButton uzyjPigmentuButton;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
