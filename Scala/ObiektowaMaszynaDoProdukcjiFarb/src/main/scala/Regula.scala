package main.scala

import scala.beans.{BeanProperty, BooleanBeanProperty}

//remove if not needed
import scala.collection.JavaConversions._

class Regula(@BeanProperty var farbaWejsciowa: String,
             @BeanProperty var farbaWyjsciowa: String,
             @BeanProperty var znakToksycznosc: String,
             @BeanProperty var toksycznosc: Double,
             @BeanProperty var znakJakosc: String,
             @BeanProperty var jakosc: Double) {
  
  // Reguły są te same jeśli mają te same farby wejściowe (zakładamy, że obiekt reguły jest już przypisany do pigmentu)
  override def equals(o: Any) = o match {
    case that: Regula => that.farbaWejsciowa.equals(this.farbaWejsciowa)
    case _ => false
  }
  
  override def toString(): String = farbaWejsciowa + " -> " + farbaWyjsciowa

  private val znaki: Array[String] = Array("x", "+", "-")
    
  private def czyPoprawneZnaki(): Boolean = znaki.contains(znakToksycznosc) && znaki.contains(znakJakosc)
  
  private def czyWZakresie(): Boolean = toksycznosc >= 0 && toksycznosc <= 100 && jakosc >= 0 && jakosc <= 100
  
  def czyDobrzeSkonfigurowana(): Boolean = czyPoprawneZnaki() && czyWZakresie()
}