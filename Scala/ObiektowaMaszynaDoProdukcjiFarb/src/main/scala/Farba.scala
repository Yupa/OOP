package main.scala

import scala.beans.{BeanProperty, BooleanBeanProperty}

class Farba(@BeanProperty var kolor: String,
            @BeanProperty var toksycznosc: Double,
            @BeanProperty var jakosc: Double) {

  override def toString(): String = kolor

  // Farby są takie same jeśli mają ten sam kolor
  override def equals(o: Any) = o match {
    case that: Farba => that.kolor.equals(this.kolor)
    case _ => false
  }
  
  // Funkcja wypisująca właściwości farby
  def wypisz(): String =
    "Kolor: " + kolor + ", toksycznosc: " + toksycznosc + ", jakosc: " + jakosc

  // Tablice znaków do sprawdzania poprawności
  private val litery: Array[Char] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZąćęłńóśźżĄĆĘŁŃÓŚŹŻ.".toCharArray()
  private val cyfry: Array[Char] = "1234567890".toCharArray()
  
  private def czyZaczynaSieLitera(): Boolean = litery contains kolor.charAt(0)

  private def czyPoprawnaNazwaFarby(): Boolean = {
    var res = true
    if (!czyZaczynaSieLitera()) 
      res = false
    for ( c <- kolor ) {
      if (res && !litery.contains(c) && !cyfry.contains(c) && !c.equals('-')) 
        res = false
    }
    res
  }
  
  private def czyWZakresie() = toksycznosc >= 0 && toksycznosc <= 100 && jakosc >= 0 && jakosc <= 100
    
  // Funkcja mówiąca czy farba jest zgodna z opisem z tresci zadania
  def czyDobrzeSkonfigurowana(): Boolean = czyPoprawnaNazwaFarby() && czyWZakresie()
}
