package main.scala

import java.util.ArrayList

import java.util.List

import java.util.Objects

import scala.beans.{BeanProperty, BooleanBeanProperty}

//remove if not needed
import scala.collection.JavaConversions._

class Pigment(@BeanProperty var nazwa: String) {

  @BeanProperty
  var reguly: List[Regula] = new ArrayList[Regula]()

  def dodajRegule(r: Regula): Unit = {
    reguly.add(r)
  }

  // Pigmenty są takie same jeśli mają te same nazwy
  override def equals(o: Any) = o match {
    case that: Pigment => that.nazwa.equals(this.nazwa)
    case _ => false
  }

  override def toString(): String = nazwa
  
  // Pomocnicze tablice ze znakami do sprawdzania poprawności nazwy pigmentu
  private val litery: Array[Char] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZąćęłńóśźżĄĆĘŁŃÓŚŹŻ".toCharArray()
  private val cyfry: Array[Char] = "1234567890".toCharArray()
  
  private def czyPoprawnaNazwaPigmentu(): Boolean = {
    var res = true
    for ( c <- nazwa ) {
      if (!res && !litery.contains(c) && !cyfry.contains(c)) 
        res = false
    }
    res
  }
  
  // Sprawdzamy czy reguły są poprawne
  private def czyPoprawneReguly(): Boolean = {
    var res = true
    for ( r <- reguly )
      if (!r.czyDobrzeSkonfigurowana())
        res = false
    res
  }
  
  def czyDobrzeSkonfigurowany(): Boolean = czyPoprawnaNazwaPigmentu() && czyPoprawneReguly()
  
}
