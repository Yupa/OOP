package main.scala

import java.beans._

import java.io.Serializable

import java.util.ArrayList

import java.util.List

import java.util.Random

import scala.beans.{BeanProperty, BooleanBeanProperty}

class Magazyn extends Serializable {

  var listaFarb: List[Farba] = new ArrayList()
  var listaPigmentow: List[Pigment] = new ArrayList()
  var listaRegul: List[Regula] = new ArrayList()
  var obecnaFarba: Farba = new Farba("", 0, 0)
  var obecnyPigment: Pigment = new Pigment("")
  var obecnaRegula: Regula = new Regula("", "", "+", 0, "+", 0)

  val LISTA_FARB_PROPERTY: String = "listaFarb"
  val LISTA_PIGMENTOW_PROPERTY: String = "listaPigmentow"
  val LISTA_REGUL_PROPERTY: String = "listaRegul"
  val OBECNA_FARBA_PROPERTY: String = "obecnaFarba"
  val OBECNY_PIGMENT_PROPERTY: String = "obecnyPigment"
  val OBECNA_REGULA_PROPERTY: String = "obecnaRegula"
  
  private var listaFarbSupport: PropertyChangeSupport = new PropertyChangeSupport(this)
  private var listaPigmentowSupport: PropertyChangeSupport = new PropertyChangeSupport(this)
  private var listaRegulSupport: PropertyChangeSupport = new PropertyChangeSupport(this)
  private var obecnaFarbaSupport: PropertyChangeSupport = new PropertyChangeSupport(this)
  private var obecnyPigmentSupport: PropertyChangeSupport = new PropertyChangeSupport(this)
  private var obecnaRegulaSupport: PropertyChangeSupport = new PropertyChangeSupport(this)

  private var r: Random = new Random()
  
  // Nie chcemy defaultowych setterów więc nie możemy użyć @BeanProperty
  def getListaFarb = listaFarb  
  def setListaFarb(value: List[Farba]): Unit = {
    listaFarb = value
    listaFarbSupport.firePropertyChange(LISTA_FARB_PROPERTY, null, value)
  }
  def getListaPigmentow = listaPigmentow
  def setListaPigmentow(value: List[Pigment]): Unit = {
    listaPigmentow = value
    listaPigmentowSupport.firePropertyChange(LISTA_PIGMENTOW_PROPERTY, null, value)
  }
  def getListaRegul = listaRegul
  def setListaRegul(value: List[Regula]): Unit = {
    listaRegul = value
    if(!value.isEmpty())
      setObecnaRegula(value.get(0))
    listaRegulSupport.firePropertyChange(LISTA_REGUL_PROPERTY, null, value)
  }
  def getObecnaFarba = obecnaFarba
  def setObecnaFarba(value: Farba): Unit = {
    obecnaFarba = value
    obecnaFarbaSupport.firePropertyChange(OBECNA_FARBA_PROPERTY, null, obecnaFarba)
  }
  def getObecnyPigment = obecnyPigment
  def setObecnyPigment(value: Pigment): Unit = {
    obecnyPigment = value
    setListaRegul(obecnyPigment.getReguly)
    obecnyPigmentSupport.firePropertyChange(OBECNY_PIGMENT_PROPERTY, null, obecnyPigment)
  }
  def getObecnaRegula = obecnaRegula
  def setObecnaRegula(value: Regula): Unit = {
    obecnaRegula = value
    obecnaRegulaSupport.firePropertyChange(OBECNA_REGULA_PROPERTY, null, obecnaRegula)
  }

  // Dodajemy listenery do odpowiednich atrybutów
  def addListaFarbChangeListener(listener: PropertyChangeListener): Unit = 
    listaFarbSupport.addPropertyChangeListener(listener)
  def removeListaFarbChangeListener(listener: PropertyChangeListener): Unit = 
    listaFarbSupport.removePropertyChangeListener(listener)
  def addListaPigmentowChangeListener(listener: PropertyChangeListener): Unit = 
    listaPigmentowSupport.addPropertyChangeListener(listener)
  def removeListaPigmentowChangeListener(listener: PropertyChangeListener): Unit =
    listaPigmentowSupport.removePropertyChangeListener(listener)
  def addListaRegulChangeListener(listener: PropertyChangeListener): Unit =
    listaRegulSupport.addPropertyChangeListener(listener)
  def removeListaRegulChangeListener(listener: PropertyChangeListener): Unit =
    listaRegulSupport.removePropertyChangeListener(listener)
  def addObecnaFarbaChangeListener(listener: PropertyChangeListener): Unit =
    obecnaFarbaSupport.addPropertyChangeListener(listener)
  def removeObecnaFarbaChangeListener(listener: PropertyChangeListener): Unit =
    obecnaFarbaSupport.removePropertyChangeListener(listener)
  def addObecnyPigmentChangeListener(listener: PropertyChangeListener): Unit =
    obecnyPigmentSupport.addPropertyChangeListener(listener)
  def removeObecnyPigmentChangeListener(listener: PropertyChangeListener): Unit =
    obecnyPigmentSupport.removePropertyChangeListener(listener)
  def addObecnaRegulaChangeListener(listener: PropertyChangeListener): Unit =
    obecnaRegulaSupport.addPropertyChangeListener(listener)
  def removeObecnaRegulaChangeListener(listener: PropertyChangeListener): Unit =
    obecnaRegulaSupport.removePropertyChangeListener(listener)
  
  // Funkcja dodająca farbę do magazynu, zwraca true jeśli się udało
  def dodajFarbe(f: Farba): Boolean = {
    // Jeśli farba istnieje to nie dodajemy
    if (listaFarb.contains(f)) false
    else {
      listaFarb.add(f)
      setListaFarb(listaFarb)
      true
    }
  }

  // Funkcja dodająca pigment do magazynu, zwraca true jeśli się udało
  def dodajPigment(p: Pigment): Boolean = {
    if (listaPigmentow.contains(p)) {
      // Jeśli pigment istnieje to dodajemy tylko regułę z przekazanego pigmentu
      val listaRegul = listaPigmentow.get(listaPigmentow.indexOf(p)).getReguly()
      val r = p.getReguly().get(0)
      if(listaRegul.contains(r))
          false
      else {
        listaRegul.add(r)
        true
      }
          true
    }
    else {
      listaPigmentow.add(p)
      setListaPigmentow(listaPigmentow)
      true
    }
  }
  
  // Funkcja pomocnicza losująca nazwę
  private def losujNazwe(): String = {
    val dlugosc: Int = r.nextInt(7) + 4
    val chars: Array[Char] = "abcdefghijklmnopqrstuvwxyz".toCharArray()
    val sb: StringBuilder = new StringBuilder()
    for (i <- 0 until dlugosc) {
      val c: Char = chars(r.nextInt(chars.length))
      sb.append(c)
    }
    val output: String = sb.toString
    output
  }

  // Funkcja pomocnicza losująca znak ze zbioru (+, -, x)
  private def losujZnak(): String = {
    val jakiZnak: Int = r.nextInt(3)
    jakiZnak match {
      case 0 => "+"
      case 1 => "-"
      case _ => "x"
    }
  }
  
  // Funkcja dodająca losową regułę do przekazanego pigmentu
  def stworzLosowaRegule(p : Pigment) = {
    val znakToksycznosci: String = losujZnak()
    val znakJakosci: String = losujZnak()
    val toksycznosc: Double = r.nextDouble() * 100
    val jakosc: Double = r.nextDouble() * 100
    val nowaRegula: Regula = new Regula(losujNazwe(), losujNazwe(), znakToksycznosci, toksycznosc, znakJakosci, jakosc)
    p.dodajRegule(nowaRegula)
    // Uaktualniamy listy oraz umożliwiamy edycję nowej reguły
    setListaRegul(p.getReguly())
    setObecnaRegula(nowaRegula)
  }

  // Funkcja dodająca losowy pigment o unikalnej nazwie do listy pigmentów
  def stworzLosowyPigment() = {
    val nazwa: String = losujNazwe()
    val nowyPigment: Pigment = new Pigment(nazwa)
    while ( listaPigmentow.contains(nowyPigment) ) {
      nowyPigment.setNazwa(losujNazwe())
    }
    // Dodajemy losową regułę do nowego pigmentu
    stworzLosowaRegule(nowyPigment)
    // Aktualizujemy listę oraz umożliwiamy edycję pigmentu
    listaPigmentow.add(nowyPigment)
    setListaPigmentow(listaPigmentow)
    setObecnyPigment(nowyPigment)
  }

  // Funkcja dodająca losową farbę o unikalnej nazwie do listy farb
  def stworzLosowaFarbe() = {
    val r: Random = new Random()
    val nazwa: String = losujNazwe()
    val toksycznosc: Double = r.nextInt(101)
    val jakosc: Double = r.nextInt(101)
    var nowaFarba = new Farba(nazwa, toksycznosc, jakosc)
    while ( listaFarb.contains(nowaFarba) ) {
      nowaFarba.setKolor(losujNazwe())
    }
    // Umożliwiamy edycję nowej farby oraz dodajemy ją do listy farb. Aktualizujemy listy
    listaFarb.add(nowaFarba)
    setListaFarb(listaFarb)
    setObecnaFarba(nowaFarba)
  }

}
