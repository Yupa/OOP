package main.scala

import main.java.Maszyna
import java.util.List
import scala.collection.JavaConversions._

class SymulacjaMaszyny extends Maszyna {

  def mieszaj(farba: Farba): Unit = {
    println("Zaczynam mieszanie. Farba: " + farba.wypisz())
  }

  // Zakładamy, że zawsze zostajemy w zakresie 0, 100
  private def oblicz(wejscie: Double, znak: String, zmiana: Double): Double = znak match {
    case "+" => Math.min(wejscie + zmiana, 100)
    case "-" => Math.max(wejscie - zmiana, 0)
    case "x" => Math.min(wejscie * zmiana, 100)
    case _ => 0
  }

  // Zwraca true jeśli udało się zmieszać pigment z farbą
  def uzyjPigmentu(farba: Farba, pigment: Pigment): Boolean = {
    var wybranaRegula: Regula = null
    // Szukamy odpowiedniej reguły (bierzemy losową jeśli jest więcej niż jedna)
    for (r <- pigment.getReguly() if r.getFarbaWejsciowa == farba.getKolor) {
      wybranaRegula = r
    }
    // Jeśli nie ma reguły to zwracamy false
    if (wybranaRegula == null) 
      false
    else {
      farba.setKolor(wybranaRegula.getFarbaWyjsciowa)
      farba.setJakosc(oblicz(farba.getJakosc, wybranaRegula.getZnakJakosc, wybranaRegula.getJakosc))
      farba.setToksycznosc(oblicz(farba.getToksycznosc, wybranaRegula.getZnakToksycznosc, wybranaRegula.getToksycznosc))
      println("Dodaję pigment: " + pigment)
      println("Otrzymałem farbę: " + farba.wypisz())
      true
    }
  }

  def zakonczMieszanie(): Unit = {
    println("Koniec mieszania")
  }

}
