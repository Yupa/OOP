package main.scala

import main.java.Maszyna
import java.io.BufferedReader
import java.io.File
import java.io.FileNotFoundException
import java.io.FileReader
import java.io.IOException
import java.util.InputMismatchException
import java.util.Locale
import java.util.NoSuchElementException
import java.util.Scanner
import scala.util.control.Breaks._
import resource._

class KonfiguracjaMaszyny {
  var maszyna: Maszyna = _
  private var farbyFile: String = _
  private var pigmentyFile: String = _
  private val CONFIGURATION_FILE: String = "maszyna.conf"
  
  def getMaszyna(): Maszyna = maszyna

  // Wstępna konfiguracja, odczytywanie pliku maszyna.conf
  def wczytaj(): Boolean = {
    var res = true
    val fileName: String = CONFIGURATION_FILE
    var line: String = null
    try {
      val fileReader: FileReader = new FileReader(fileName)
      val bufferedReader: BufferedReader = new BufferedReader(fileReader)
      line = bufferedReader.readLine()
      if (line != null) 
        farbyFile = line
      else 
        res = false
      line = bufferedReader.readLine()
      if (line != null) 
        pigmentyFile = line
      else 
        res = false
      bufferedReader.close()
    } catch {
      case ex: FileNotFoundException => {
        println("Unable to open file '" + fileName + "'")
        res = false
      }
      case ex: IOException => {
        println("Error reading file '" + fileName + "'")
        res = false
      }

    }
    // Tutaj wczytywalibyśmy maszynę
    maszyna = new SymulacjaMaszyny()
    // Zwracamy czy konfiguracja się powiodła
    res
  }
  
  private def wczytajFarby(magazyn: Magazyn): Boolean = {
    var res = true
    try {
      // managed z biblioteki scala-arm jest odpowiednikiem javovego try with resources
      for (scan <- managed(new Scanner(new File(farbyFile))))
        while (scan.hasNextLine()) {
          val s: String = scan.nextLine()
          for (scan2 <- managed(new Scanner(s).useLocale(Locale.US))) {
            val kolor: String = scan2.next()
            val toksycznosc: Double = scan2.nextDouble()
            val jakosc: Double = scan2.nextDouble()
            val nowaFarba = new Farba(kolor, toksycznosc, jakosc)
            // Sprawdzamy czy farba jest zgodna z opisem oraz czy sie nie powtarza w magazynie
            if(!nowaFarba.czyDobrzeSkonfigurowana() || !magazyn.dodajFarbe(nowaFarba))
              res = false
          }
        }
    } 
    catch {
      case e: FileNotFoundException => {
        println("Nie ma takiego pliku")
        res = false
      }

      case e: IllegalStateException => {
        println("Scanner został zamknięty")
        res = false
      }

      case e: InputMismatchException => {
        println("Błędne dane wejściowe farb")
        res = false
      }

      case e: NoSuchElementException => {
        println("Koniec pliku")
        res = false
      }

    }
    if(res)
      println("Wczytano plik z farbami")
    else
      println("Błędny plik z farbami")
    res
  }

  private def wczytajPigmenty(magazyn: Magazyn): Boolean = {
    var res = true
    try {
      for (scan <- managed(new Scanner(new File(pigmentyFile)))) {
        while (scan.hasNextLine()) {
          val s: String = scan.nextLine()
          for (scan2 <- managed(new Scanner(s).useLocale(Locale.US))) {
            val nazwa: String = scan2.next()
            val jakiKolor: String = scan2.next()
            val naJakiKolor: String = scan2.next()
            val jakiZnakToksycznoscString: String = scan2.findInLine(" .")
            // Sprawdzamy czy findInLine sie dopasowal, jesli nie to wyjatek
            if (jakiZnakToksycznoscString == null)
              throw new InputMismatchException()
            val jakiZnakToksycznosc: Char = jakiZnakToksycznoscString.charAt(1)
            val toksycznosc: Double = scan2.nextDouble()
            val jakiZnakJakoscString: String = scan2.findInLine(" .")
            if (jakiZnakJakoscString == null)
              throw new InputMismatchException()
            val jakiZnakJakosc: Char = jakiZnakJakoscString.charAt(1)
            val jakosc: Double = scan2.nextDouble()
            val p: Pigment = new Pigment(nazwa)
            p.dodajRegule(new Regula(jakiKolor, naJakiKolor, "" + jakiZnakToksycznosc, toksycznosc, "" + jakiZnakJakosc, jakosc))
            // Sprawdzamy czy pigment jest zgodny z opisem oraz czy nie powtarza sie w magazynie
            if (!p.czyDobrzeSkonfigurowany() || !magazyn.dodajPigment(p))
              res = false
          }
        }
      }
    } catch {
      case e: FileNotFoundException => {
        println("Nie ma takiego pliku")
        res = false
      }
      case e: IllegalStateException => {
        println("Scanner został zamknięty")
        res = false
      }
      case e: InputMismatchException => {
        println("Błędne dane wejściowe pigmentów")
        res = false
      }
      case e: NoSuchElementException => {
        println("Koniec pliku")
        res = false
      }

    }
    if(res)
      println("Wczytano plik z pigmentami")
    else
      println("Błędny plik z pigmentami")
    res
  }

  def wczytajFarbyIPigmenty(magazyn: Magazyn): Boolean =
    wczytajFarby(magazyn) && wczytajPigmenty(magazyn)

}
