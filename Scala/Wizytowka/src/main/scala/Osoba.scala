
package main.scala

class Osoba {
  private var imie: String = ""
  private var nazwisko: String  = ""
  private var wiek: String  = ""
  private var pesel: String  = ""
  private var plec: String  = ""
  private val rnd = new scala.util.Random
  
  // gettery i settery
  def getImie = imie
  def getNazwisko = nazwisko
  def getWiek = wiek
  def getPesel = pesel
  def getPlec = plec
  def setImie(value:String):Unit = imie = value
  def setNazwisko(value:String):Unit = nazwisko = value
  def setWiek(value:String):Unit = wiek = value
  def setPesel(value:String):Unit = pesel = value
  def setPlec(value:String):Unit = plec = value
    
  // funkcja losująca wiek
  private def losujWiek() {
      wiek = "" + (rnd.nextInt(120) + 1);
  }
    
  // funkcja losująca pesel
  private def losujPesel() {
      var s = "";
      for (a <- 0 to 11) {
          s += rnd.nextInt(10);
      }
      pesel = s;
  }
  
  // funkcja losująca imię
  private def losujImie() {
      if(imie.length() == 0)
          return;
      val zmien = rnd.nextInt(imie.length());
      var s = "";
      for(a <- 0 to imie.length() - 1) {
          if(a == zmien) 
            s += (rnd.nextInt(26) + 'a').toChar
          else
            s += imie.charAt(a)
      }
      imie = s;
  }
  
  // funkcja losująca nazwisko
  private def losujNazwisko() {
      if(nazwisko.length() == 0)
          return;
      val zmien = rnd.nextInt(nazwisko.length());
      var s = "";
      for(a <- 0 to nazwisko.length() - 1) {
          if(a == zmien) 
            s += (rnd.nextInt(26) + 'a').toChar
          else
            s += nazwisko.charAt(a)
      }
      nazwisko = s;
  }
  
  // funkcja losująca płeć
  def losujPlec() {
      if(rnd.nextInt(2) == 0)
          plec = "Mężczyzna";
      else
          plec = "Kobieta";
  }
  
  // funkcja losująca dane
  def losuj () {
      losujImie();
      losujNazwisko();
      losujWiek();
      losujPesel();
      losujPlec();
  }
  
  override def toString = imie + " " + nazwisko + ", wiek: " + wiek + ", płeć: " + plec + ", pesel: " + pesel
}