object Cwiczenia1 {
	//println("Hello World!")

  val c = new Cwiczenia1Funkcje()                 //> c  : Cwiczenia1Funkcje = Cwiczenia1Funkcje@50134894
	c.sum(x=>x*x)(1,3)                        //> res0: Double = 14.0
	c.product(x=>x)(3,5)                      //> res1: Double = 60.0
	c.silnia(6)                               //> res2: Double = 720.0
	c.sum2(x=>x*x)(1,3)                       //> res3: Double = 14.0
	c.product2(x=>x)(3,5)                     //> res4: Double = 60.0
	
}