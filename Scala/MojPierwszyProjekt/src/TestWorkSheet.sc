object TestWorkSheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  1                                               //> res0: Int(1) = 1
  1+2                                             //> res1: Int(3) = 3
  
  var x = 3                                       //> x  : Int = 3
  var y = 2                                       //> y  : Int = 2
  y += x
  println(y)                                      //> 5
}