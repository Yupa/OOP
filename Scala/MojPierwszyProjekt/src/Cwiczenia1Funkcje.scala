

class Cwiczenia1Funkcje {
  
  //1
	def sum(f: Int => Double)(a: Int, b: Int): Double = {
	  def iter(a: Int, result: Double): Double = {
	    if (a > b) result
	    else iter(a + 1, result + f(a))
	  }
	  iter(a, 0)
	}
	
	//2 Napisz funkcję product, która oblicza iloczyn wartości zadanej funkcji w punktach z podanego zakresu.
	def product(f: Int => Double)(a: Int, b: Int): Double = {
	  def iter(a: Int, result: Double): Double = {
	    if (a > b) result
	    else iter(a + 1, result * f(a))
	  }
	  iter(a, 1)
	}
	
	//3 Przy pomocy rozwiązania poprzedniego zadania zdefiniuj silnię.
	def silnia(a: Int): Double = product(x => x)(1, a)
	
	//4 Napisz funkcję, która uogólnia zarówno produkt jak i sumę (najlepiej z rekrusją ogonową).
	def operator(op : (Double, Double) => Double)(f: Int => Double)(a: Int, b: Int): Double = {
	  def iter(a: Int, result: Double): Double = {
	    if (a > b) result
	    else iter(a + 1, op(result, f(a)))
	  }
	  iter(a + 1, f(a))
	}
	def sum2(f: Int => Double)(a: Int, b: Int): Double = {
	  operator((x,y) => x + y)(f)(a, b)
	}
	def product2(f: Int => Double)(a: Int, b: Int): Double = {
	  operator((x,y) => x * y)(f)(a, b)
	}
	
	
}