object TEST {
  def main(args: Array[String]) {
    val bazaDanych = new BazaDanych()
    // Indeksy od 0
    bazaDanych.dodajGenom("TTTAGGACCGGGTAATAAAAAAAAAAAAAAAAAAAAA", 
        //LTR (poczatek, koniec)
        List((0,2), (4,5)),
        //non-LTR (poczatek, koniec)
        List((7,8)),         
        //Autonomiczne (poczatek, koniec, nazwa bialka, poczatek genu, koniec genu)
        List((12,15,"białko1",13,14)),
        //Nieautonomiczne (poczatek, koniec)
        List((9,11))
    )  
    bazaDanych.dodajGenom("ACCCAGGGCCGAATTTTTAAAAAAAAAAAAAAAAAAAAAAAAAA", 
        List((1,3)), 
        List(), 
        List((5,10,"białko2",8,9)), 
        List((13,17))
    )
    
    bazaDanych.dodajGenom("ACCCAGGGCCGAATTTTTAAAAAAAAAAAAAAAAAAAAAAAAAA", 
        List((1,3), (4,5), (7,8), (10,11), (12,13)), 
        List(), 
        List(), 
        List()
    )
    bazaDanych.wypiszGenomy
    
    println("\nCzy chcesz zrobić ewolucję? 0/1")
    var czy = scala.io.StdIn.readInt()
    while (czy != 0) {
      println("Podaj numer genomu a do a.doTheEvolution(b)")
      val a=scala.io.StdIn.readInt()
      println("Podaj numer genomu b do a.doTheEvolution(b)")
      val b=scala.io.StdIn.readInt()
      bazaDanych.wykonajEwolucje(a, b)
      bazaDanych.wypiszGenomy
      
      println("\nCzy chcesz zrobić ewolucję? 0/1")
      czy = scala.io.StdIn.readInt()
    }
  }
}
