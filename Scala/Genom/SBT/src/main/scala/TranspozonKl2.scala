

abstract class TranspozonKl2(a: Boolean, g: Genom, p: CzłonListy[Zasada], k: CzłonListy[Zasada]) 
extends Transpozon(a, g, p, k) {
  // cut and paste
  def przemieszczaj(b: Genom): Unit = {
    val nowaPozycja = losujNowaPozycje
    g.przenieś(nowaPozycja, this)
  }
}