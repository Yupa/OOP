
abstract class TranspozonKl1(a: Boolean, g: Genom, p: CzłonListy[Zasada], k: CzłonListy[Zasada]) 
extends Transpozon(a, g, p, k) {
  // copy and paste
  def przemieszczaj(b: Genom): Unit = {
    val nowaPozycja = losujNowaPozycje
    g.skopiuj(nowaPozycja, this)
  }
}