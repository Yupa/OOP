import scala.beans.{BeanProperty, BooleanBeanProperty}

abstract class Transpozon(@BeanProperty var aktywny: Boolean, g: Genom, p: CzłonListy[Zasada], k: CzłonListy[Zasada]) 
extends WycinekŁańcuchaDNA(g, p, k) {
  def przemieszczaj(b: Genom): Unit
  
  // zwraca wskaznik na nowa pozycje z zakresu (0, p-1) lub (b+1, n)
  def losujNowaPozycje(): CzłonListy[Zasada] = {
    val r = scala.util.Random
    val pozPocz = g.dajPoz(p)
    val pozKon = g.dajPoz(k)
    val dlug = pozKon - pozPocz + 1
    val nowaPozycja = r.nextInt(g.dajDlugosc - dlug)
    if (nowaPozycja >= pozPocz) g.dajCzłon(nowaPozycja + dlug)
    else g.dajCzłon(nowaPozycja)
  }
}