import scala.beans.{BeanProperty, BooleanBeanProperty}

abstract class CzłonListy[A](@BeanProperty var next: CzłonListy[A],
                 @BeanProperty var prev: CzłonListy[A]) {
  
}