

class TranspozonNonLTR(a: Boolean, g: Genom, p: CzłonListy[Zasada], k: CzłonListy[Zasada]) 
extends TranspozonKl1(a, g, p, k) {
  // zmieniamy genom
  override def przemieszczaj(b: Genom): Unit = {
    val r = scala.util.Random
    val nowaPozycja = r.nextInt(b.dajDlugosc)
    b.skopiuj(b.dajCzłon(nowaPozycja), this)
  }
}