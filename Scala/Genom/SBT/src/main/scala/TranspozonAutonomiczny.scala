import scala.beans.{BeanProperty, BooleanBeanProperty}

class TranspozonAutonomiczny(@BeanProperty var gen: Gen, a: Boolean, g: Genom, p: CzłonListy[Zasada], k: CzłonListy[Zasada]) 
extends TranspozonKl2(a, g, p, k) {
  // z szansa 2/3 wycinamy gen
  override def przemieszczaj(b: Genom): Unit = {
    val r = scala.util.Random
    if (r.nextInt(3) < 2)
      wytnijGen
    super.przemieszczaj(b)
  }
  
  def wytnijGen = {
    if (gen != null)
      g.wytnij(gen)
    setGen(null)
  }
}